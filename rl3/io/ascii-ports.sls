(library
 (rl3 io ascii-ports)
 
 (export make-ascii-input-port
	 make-ascii-output-port)
 
 (import
  (rnrs base)
  (rnrs arithmetic fixnums)
  (only (rnrs io ports)
	make-transcoder
	transcoded-port
	utf-8-codec
	get-u8 put-u8
	close-port eof-object?
	make-custom-textual-input-port
	make-custom-textual-output-port)
  (only (rnrs mutable-strings)
	string-set!)
  (only (rl3 env prelude)
	fx1+ fx1-))
 
 ;; Temporary simple ascii textual port implementation that is a work around
 ;; for transcoded-port bug in Larceny.
 ;;
 ;; binary-input-port? -> textual-input-port?
;;  (define make-ascii-input-port
;;    (lambda (ip)
;;      (let ((read! (lambda (str start count)
;; 		  (let loop ((idx start) (cnt 0))
;; 		    (if (fx<? cnt count)
;; 		       (let ((u8 (get-u8 ip)))
;; 			 (if (eof-object? u8)
;; 			    cnt
;; 			    (begin
;; 			      (string-set! str idx (integer->char u8))
;; 			      (loop (fx1+ idx) (fx1+ cnt)))))
;; 		       cnt))))
;; 	 (close (lambda ()
;; 		  (close-port ip))))
;;        (make-custom-textual-input-port "ascii input port" read! #f #f close))))

 (define make-ascii-input-port
   (lambda (ip)
     (transcoded-port ip (make-transcoder (utf-8-codec)))))
 
 (define make-ascii-output-port
   (lambda (op)
     (let ((write! (lambda (str start count)
		   (let loop ((idx start) (cnt count))
		     (if (fxzero? cnt)
			count
			(begin
			  (put-u8 op (char->integer (string-ref str idx)))
			  (loop (fx1+ idx) (fx1- cnt)))))))
	 (close (lambda ()
		  (close-port op))))
       (make-custom-textual-output-port "ascii output port" write! #f #f close))))
)


