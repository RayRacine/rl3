(library
  (rl3 io epoll constants)
  
  (export
   EPOLLIN EPOLLPRI EPOLLOUT
   EPOLLRDNORM EPOLLRDBAND
   EPOLLWRNORM EPOLLWRBAND
   EPOLLMSG EPOLLERR
   EPOLLONESHOT EPOLLET
   
   EPOLL-CTL-ADD EPOLL-CTL-DEL EPOLL-CTL-MOD
   EPOLL-EVENT-EVENTS EPOLL-EVENT-DATA
   EPOLL-SIZEOF-STRUCT-EPOLL-EVENT EPOLL-SIZEOF-EPOLL-DATA)
  
  (import 
   (rnrs base)
   (rnrs io simple)
   (only (rl3 io print)
         format)
   (for (rl3 ffi foreign-ctools)
        run expand))
  
  (define-c-info (include<> "sys/epoll.h")
    ;; EPoll Events
    (const EPOLLIN      int "EPOLLIN")
    (const EPOLLPRI     int "EPOLLPRI")
    (const EPOLLOUT     int "EPOLLOUT")
    (const EPOLLRDNORM  int "EPOLLRDNORM")
    (const EPOLLRDBAND  int "EPOLLRDBAND")
    (const EPOLLWRNORM  int "EPOLLWRNORM")
    (const EPOLLWRBAND  int "EPOLLWRBAND")  
    (const EPOLLMSG     int "EPOLLMSG")
    (const EPOLLERR     int "EPOLLERR")
    (const EPOLLONESHOT int "EPOLLONESHOT")
    (const EPOLLET     uint "EPOLLET")
    
    ;; EPoll Ops
    (const EPOLL-CTL-ADD int "EPOLL_CTL_ADD")
    (const EPOLL-CTL-DEL int "EPOLL_CTL_DEL")
    (const EPOLL-CTL-MOD int "EPOLL_CTL_MOD")
    
    ;; EPoll Structures
    (sizeof EPOLL-SIZEOF-STRUCT-EPOLL-EVENT "struct epoll_event")
    (sizeof EPOLL-SIZEOF-EPOLL-DATA         "union epoll_data")
    
    (struct "epoll_event"
            (EPOLL-EVENT-EVENTS "events")
            (EPOLL-EVENT-DATA   "data")))
  
  )

(library
  (rl3 io epoll)
  
  (export
   ioevent-flags ioevent-fd
   epoll-create epoll-wait epoll-ctl epoll-close)
  
  (import
   (rnrs base)
   (only (rnrs control)
         when)
   (only (rnrs arithmetic fixnums)
         fixnum? fx+ fx<?)
   (only (rnrs bytevectors)
         bytevector-u32-native-set!
         bytevector-u32-native-ref
         make-bytevector)
   (only (rnrs io simple)
         current-output-port)
   (only (rl3 io print)
         format)
   (err5rs records syntactic)
   (only (larceny records printer)
         rtd-printer-set!)
   (only (rl3 system unix)
         unix-close)
   (only (rl3 ffi ffi-std)
         foreign-procedure)   
   (rl3 io epoll constants))
  
  (define-record-type ioevent #t #t flags fd)  
  
  (define c/epoll-create
    (foreign-procedure "epoll_create" '(int) 'int))
  
  (define c/epoll-ctl
    (foreign-procedure "epoll_ctl" '(int int int boxed) 'int))
  
  (define c/epoll-wait
    (foreign-procedure "epoll_wait" '(int boxed int int) 'int))
  
  (define EPOLL-TIMEOUT 0) ;; no wait poll
  (define EPOLL-MAX-EVENTS 16)
  (define EPOLL-CREATE-DEFAULT-SIZE 128)
  
  ;; args: size hint : int
  ;; returns: fd : int
  (define epoll-create
    (lambda size-arg
      (let ((size (if (pair? size-arg)
                      (begin
                        (when (not (fixnum? (car size-arg)))
                          (error "Size must be less then fixnum max: " size-arg))
                        (car size-arg))
                      EPOLL-CREATE-DEFAULT-SIZE)))
        (let ((fd (c/epoll-create size)))
          (when (< fd 0)
            (error "EPoll create failed"))
          fd))))

  ;; args:
  ;;   epoll-fd : file-descriptor - the fd used for the epoll set, created by epoll-create
  ;;   op : int - the operation ADD, MOD, or DEL
  ;;   fd : file-descriptor - the file descriptor
  ;;   events : listof epoll flag constants
  (define epoll-ctl
    (lambda (epoll-fd op fd events)
      (let ((event (make-bytevector EPOLL-SIZEOF-STRUCT-EPOLL-EVENT))
            (eventflgs (apply + events)))  ;; fixme logior ???
        (bytevector-u32-native-set! event EPOLL-EVENT-EVENTS eventflgs)
        (bytevector-u32-native-set! event EPOLL-EVENT-DATA fd)
        (c/epoll-ctl epoll-fd op fd event))))

;;          (event-mask  (+ EPOLLIN EPOLLERR))) ;; EPOLLHUP WPOLLET)))

;;;
  ;; returns a list of ready fds
  
  (define epoll-wait
    (lambda (epoll-fd timeout)
      (let ((events (make-bytevector (* EPOLL-MAX-EVENTS EPOLL-SIZEOF-STRUCT-EPOLL-EVENT) 0)))
;;         (format (current-output-port)
;;                 "EPoll Wait: ~s ~s ~s~%" epoll-fd EPOLL-MAX-EVENTS timeout)
        (let ((num-events (c/epoll-wait epoll-fd events EPOLL-MAX-EVENTS timeout)))
          (let loop ((idx 0)(ready-events '()))
            (if (fx<? idx num-events)
                (let ((offset (* idx EPOLL-SIZEOF-STRUCT-EPOLL-EVENT)))
                  (let ((flgs (bytevector-u32-native-ref events (+ offset EPOLL-EVENT-EVENTS)))
                        (fd   (bytevector-u32-native-ref events (+ offset EPOLL-EVENT-DATA))))
                    (loop (fx+ idx 1) (cons (make-ioevent flgs fd) ready-events))))
                ready-events))))))
              
;; ;; START HERE 
;; 	  ;;(values num-events events)))))
   
  (define epoll-close
    (lambda (fd)
      (assert (fixnum? fd))
      (unix-close fd)))

;; ;; (begin
;; ;;   (let ((efd (epoll-create))
;; ;;         (ss (make-server-socket 6363)))
;; ;;     (let ((sfd (socket-representation-sock ss)))
;; ;;       (assert (> efd 0))
;; ;;       (assert (> sfd  0))
;; ;;       (format #t "Created epoll ~a\n" efd)
;; ;;       (format #t "Listener with fd ~a\n" sfd)
;; ;;       (epoll-ctl efd EPOLL-CTL-ADD sfd (list EPOLLIN EPOLLERR))
;; ;;       (let ((num-events (epoll-wait efd )))
;; ;;         (format #t "Polled ~a\n" num-events))
;; ;;       (epoll-close efd)
;; ;;       (unix/close sfd)
;; ;;       (format #t "Closed epoll fd ~a\n" efd))))


;; ;; (define ss (make-server-socket 6363))
;; ;; (define sfd (socket-representation-sock ss))


(rtd-printer-set! ioevent
                  (lambda (ioevent outp)
                    (format outp "<ioevent flgs:~s fd:~s>"
                            (ioevent-flags ioevent)
                            (ioevent-fd    ioevent))))
  
)
