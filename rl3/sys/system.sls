;;;
;; Drop Larceny system information procs into R6RS library.
;; Useful for importing for expansion in FFI
;;;

(library
 (rl3 sys system)
 
 (export
  exit break getenv
  call-without-interrupts disable-interrupts enable-interrupts error-handler timer-interrupt-handler
  os-type perror reset-handler system standard-timeslice system-features)
 
 (import
  (rnrs base)
  (only (rnrs lists)
        assq member)
  (rename (only (rl3 system unix)
		unix-perror)
	  (unix-perror perror))
  (primitives exit call-without-interrupts larceny-break getenv system system-features
              enable-interrupts error-handler disable-interrupts
              reset-handler standard-timeslice timer-interrupt-handler))

 
 (define break larceny-break)
 
 (define os-type
   (let ((os-name (assq 'os-name (system-features))))
     (cond
      ((member os-name '((os-name . "Linux")
                         (os-name . "MaxOS X")
                         (os-name . "SunOS")))
       'unix)
      ((member os-name '((os-name . "Win32")))
       'windows)
      (else
       (error 'os-type ": add case for os " os-name)))))

 )

