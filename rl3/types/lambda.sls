(library
 (rl3 types lambda)

 (export
  identity)
 
 (import
  (rnrs base))
 
 (define identity
   (lambda x x)))
 