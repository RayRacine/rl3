(library
 (rl3 types lists)

 (export
  assoc-value assoc-string assoc-string-value
  list-copy lookup
  last-pair make-list every? any? weave)
  
 (import
  (rnrs base)
  (rnrs lists)
  (only (rnrs arithmetic fixnums)
        fxzero?)
  (only (rl3 env prelude)
        fx1-)
  (only (rl3 control)
	aif)
  (primitives assoc-string every? any? list-copy))

 ;; find an element in a list satisfing the given predicate
 ;; return the given default value
 (define lookup
   (lambda (proc list default)
     (aif (find proc list)
	  it
	  default)))

 (define assoc-value
   (lambda( key alst)
     (let ((pair (assoc key alst)))
       (if pair
	  (cdr pair)
	  #f))))

 (define (assoc-string-value str-key alst)
   (let ((pair (assoc-string str-key alst)))
     (if pair
	(cdr pair)
	#f)))

 ;; Returns the last pair in list.
 (define last-pair
   (lambda (x)
     (if (pair? (cdr x))
	(last-pair (cdr x))
	x)))

 ;; weave an element between a list of elements
 (define weave
   (lambda (e lst)
     (if (null? lst)
        lst
        (if (null?  (cdr lst))
	   lst
	   (cons (car lst) (cons e (weave e (cdr lst))))))))
 
 (define make-list
   (lambda (len val)
     (let loop ((len len) (lst '()))
       (if (fxzero? len)
          lst
          (loop (fx1- len) (cons val lst))))))
 
 )
