
(library
 (rl3 benchmarks webserver netio)

 (export 
  main)

 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
	string->utf8)
  (only (rnrs io simple)
	display newline)
  (only (rnrs io ports)
	open-string-input-port
	close-port
	open-bytevector-input-port)
  (only (rl3 io print)
	pretty-print)
  (only (rl3 web httpserver dispatch)
	rest-resource)
  (only (rl3 web httpserver server)
	web-server)
  (only (rl3 web uri)
	parse-uri
	uri->start-line-path-string)
  (only (rl3 web http http)
	http-invoke
	http-send-response)
  (only (rl3 io ascii-ports)
	make-ascii-input-port)
  (only (rl3 web html htmlprag)
	html->sxml)
  (only (rl3 xml sxml serializer)
	sxml->html))

 (define port 8081)

 (define url (parse-uri "http://localhost:8080/"))

 (define remote-content
   (lambda ()     
     (let-values (((hdrs hip) (http-invoke 'GET url
					 '())))
	       (let ((tip (make-ascii-input-port hip)))
		 (let ((content (html->sxml tip)))
		   (close-port tip)
		   content)))))		  
 
 (define hello-mash-resource
   (rest-resource
    (GET (lambda (require remainder input-port output-port)
	   (let ((content (sxml->html (remote-content))))
	     (http-send-response "200-OK" '()
				 output-port
				 (open-bytevector-input-port (string->utf8 content))
				 0))))))

 (define dispatch-configure
   (lambda ()
     `(("" ,hello-mash-resource))))

 (define main
   (lambda ()
     (web-server dispatch-configure port)))

  
 )
