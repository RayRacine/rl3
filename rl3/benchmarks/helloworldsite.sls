;;Simple "site" that responds with s dynamically generated simple hello page

(library
 (rl3 benchmarks helloworldsite)
 
 (export
  main)
 
 (import
  (rnrs base)
  (only (rnrs bytevectors)
	string->utf8)
  (only (rnrs io ports)
	open-bytevector-input-port)
  (only (rl3 web httpserver dispatch)
	rest-resource)
  (only (rl3 web httpserver server)
	web-server)
  (only (rl3 web http http)
	http-send-response)
  (only (rl3 xml sxml serializer)
	sxml->html))

 (define port 8080)

 (define hello-sxml
   (lambda ()
     `(*TOP*
       (html
	(head
	 (title "HelloWorld"))
	(body 
	 (p "Hello, you big beautiful world."))))))
 
 (define hello-resource
   (rest-resource
    (GET (lambda (require remainder input-port output-port)
	   (http-send-response 
	    "200-OK" '() output-port
	    (open-bytevector-input-port (string->utf8 (sxml->html (hello-sxml))))
	    0)))))
 
 (define dispatch-configure
   (lambda ()
     `((""  ,hello-resource))))
 
 (define main
   (lambda ()
     (web-server dispatch-configure port)))
 
 )
