(library
 (rl3 web http proxy)

 (export
  set-http-proxy!
  add-escape-proc!
  remove-escape-proc!
  http-escape?
  http-proxy-host
  http-proxy-port)

 (import
  (rnrs base)
  (rnrs lists)
  (rnrs io simple) ;; debug
  (rl3 io print))
 
 (define proxy-host #f)
 (define proxy-port #f)
 
 ;; alistof (symbol? . authority? * uri? -> boolean?)
 ;; We pass that authority as its already been
 ;; parsed in http-invoke.
 (define proxy-escape '())
 
 (define http-proxy-host
   (lambda ()
     proxy-host))

 (define http-proxy-port
   (lambda ()
     proxy-port))

 (define set-http-proxy!
   (lambda (host port)
     (set! proxy-host host)
     (set! proxy-port port)))

 (define add-escape-proc!
   (lambda (symbol proc)
     (set! proxy-escape (cons (cons symbol proc) 
			   proxy-escape))))
 
 (define remove-escape-proc!
   (lambda (symbol)
     (set! proxy-escape (filter (lambda (esc)
			       (not (eq? symbol (car esc))))
			     proxy-escape))))

;; Determine if a http request should use the proxy or not.
;; true -> do not use proxy
;; false -> use proxy
(define http-escape?
  (lambda (authority uri)
    (let loop ((escapes proxy-escape))
      (if (null? escapes)
	 #f
	 (if ((cdr (car escapes)) authority uri)
	    #t
	    (loop (cdr escapes)))))))
 
 )
