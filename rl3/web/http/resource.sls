;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Asynchronous fetching of an HTTP resource
;; using ivars (futures).
;; Tasking MUST be enabled.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(library 
 (rl3 web http resource)

 (export
  future-resource-xml
  fetch-resource-xml)

 (import
  (rnrs base)
  (rnrs exceptions)
  (rnrs io ports)
  (rnrs conditions)

  (rnrs io simple) ;; debug

  (only (rl3 concurrency tasks)
	make-task task-start!)
  (only (rl3 concurrency ivars)
	make-ivar ivar-put)
  (only (rl3 web uri)
	uri-authority authority-host)
  (only (rl3 web http headers)
	agent-header
	host-header)
  (only (rl3 web http http)
	parse-http-response-line
	response-line-code
	http-invoke)
  (only (rl3 xml ssax ssax)
	xml->sxml))

 ;; Asynchronous fetch of a XML resourse.
 ;; Returns SXML of fetched XML represented resource.
 ;; Note: Need to build up to a a synch. fetch-resource call
 ;; for a generic resource which returns the appropriate type
 ;; based on the mime type.
 (define fetch-resource-xml
   (lambda (url headers error-proc)    
     (let ((headers (append `(,(host-header (authority-host (uri-authority url)))
			    ,(agent-header "SOS/RL3/0.1")
			    "Accept: */*"))))
       (let-values (((resp-data hip) (http-invoke 'GET url headers)))
	 (let ((tip (transcoded-port hip (make-transcoder (utf-8-codec)))))
	   (call/cc (lambda (k)
		      (with-exception-handler
		       (lambda (ec)
			 (close-port tip)
			 (error-proc))
		       (lambda ()
			 (let ((result (let ((http-resp (parse-http-response-line (car resp-data))))
				       (if (string=? (response-line-code http-resp) "200")
					  (xml->sxml tip '())
					  (error-proc)))))
			   (close-port tip)
			   result))))))))))

 ;; return a resource via HTTP GET
 ;; returns the value of invoking error-proc on an error
 (define future-resource-xml
   (lambda (url headers error-proc)
     (future-resource (lambda ()
			(fetch-resource-xml url headers error-proc)))))

 ;; Fetches content into an ivar on its a dedicated task
 ;; returns: ivar
 (define future-resource
   (lambda (fetch-thunk)
     (let ((ivar (make-ivar)))
       (task-start! (make-task (lambda ()
				 (ivar-put ivar (fetch-thunk)))))
       ivar)))
 )

