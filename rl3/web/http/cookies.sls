
#| HTTP Cookies |#

(library
 (rl3 web http cookies)
 
 (export
  make-cookie
  parse-cookie)
 
 (import 
  (rnrs base)
  (rnrs io ports)
  (only (rl3 io print)
	pretty-print)  ;; debug only
  (only (rl3 types chars)
	char-set-complement
	list->char-set)
  (only (rl3 web uri url parms)
	parse-parms)
  (only (rl3 types strings)
	string-trim
	string-tokenize)
  (only (rl3 types dates)
	current-time
	make-time 
	time-duration
	add-duration
	time->rfc2822)
  (primitives reset-output-string open-output-string get-output-string))
 
 (define cookie-delims
   (char-set-complement (list->char-set '(#\; #\=))))
 
 (define make-cookie
   (lambda (domain path name value expire-secs)
     (let ((duration (make-time time-duration 0 expire-secs)))
       (let ((expire-date (time->rfc2822 (add-duration (current-time) duration))))
	 (string-append name "=" value "; "
			"Expires=" expire-date "; "
			"Path="    path "; "
			"Domain="  domain)))))

 
 (define parse-cookie
   (lambda (cookie)
     (if (string? cookie)
	(let ((is (open-string-input-port cookie))
	    (os (open-output-string)))
	  (let loop ((avs '()) (ws #f) (attr #f))
	    (let ((ch (get-char is)))
	      (cond 
	       ((eof-object? ch)
		(if attr
		   (cons (cons attr (get-output-string os)) avs)
		   avs))
	       ((and (not ws) (char=? ch #\space))
		(loop avs ws attr))
	       (else
		(if attr
		   (if (char=? ch #\;)
		      (let ((value (get-output-string os)))
			(reset-output-string os)
			(loop (cons (cons attr value) avs) #f #f))
		      (let ((ws (if (char=? ch #\")
				 (not ws)
				 #t)))
			(put-char os ch)
			(loop avs ws attr)))
		   (if (char=? ch #\=)
		      (let ((attr (get-output-string os)))
			(reset-output-string os)
			(loop avs ws attr))
		      (begin
			(put-char os ch)
			(loop avs ws attr)))))))))
	'())))

 
 )
