
(library
 (rl3 web http encoding)
 
 (export
  parse-x-www-form-urlencoded)
 
 (import
  (rnrs base)
  (rnrs io simple) ;; debug
  (rnrs io ports)
  (rnrs control)
  (only (rl3 types chars)
	char-set-complement
	list->char-set)
  (only (rl3 web uri)
	url-decode-string)
  (primitives open-output-string
	      get-output-string))

 (define key-delim #\=)
 (define value-delim #\&)
 
 (define read-token
   (lambda (ip delim)
     (url-decode-string ip delim #t))) ;; decode + as space
 
 ;; parse port contents into an alist of (k . v) pairs
 (define parse-x-www-form-urlencoded
   (lambda (in-port)
     (let loop ((key #f) (kvs '()))
       (if key
	  (loop #f (cons (cons key (read-token in-port value-delim)) kvs))
	  (let ((key (read-token in-port key-delim)))
	    (if (string=? key "")
	       kvs
	       (loop key kvs)))))))

)
