(library
 (rl3 web httpserver response-codes)

 (export)
 
 (import (rnrs base))

;;; 400 Response Codes
 (define BAD-REQUEST-400         "400")
 (define UNAUTHORIZED            "401")
 (define FORBIDDEN               "403")
 (define REQUEST-URI-TOO-LONG    "414")

;;; 500 Response Codes
 (define SERVICE-UNAVAILABLE-501 "501")
 

 )