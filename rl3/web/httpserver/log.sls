(library
 (rl3 web httpserver log)
 
 (export
  www-log)
 
 (import 
  (rnrs base)
  (only (rnrs control)
	when)
  (only (rl3 io print)
	format)
  (only (rl3 concurrency tasks)
	current-task))
 
 (define *www-log*  #t)		 ;; #f or #t
 
 (define-syntax www-log
   (syntax-rules ()
     ((_ format-str args ...)
      (when *www-log*
	    (format #t (string-append "~s: " format-str) (current-task) args ...)))))
 )
