(library
 (rl3 web httpserver server)
 
 (export web-server)
 
 (import
  (rnrs base)
  (only (rnrs lists)
        assoc remq)
  (only (rnrs io simple)
        output-port?
        call-with-input-file
        current-output-port 
        close-input-port close-output-port
        display newline eof-object?)
  (only (rnrs io ports)
        get-bytevector-some get-bytevector-n get-u8 get-line
        put-bytevector put-u8
        flush-output-port)
  (only (rnrs control)
        do when)
  (only (rnrs arithmetic fixnums)
        fx=?)
  (only (rnrs unicode)
        char-whitespace?
        string-ci=?)
  (only (rnrs mutable-strings)
        string-set!)
  (only (rnrs bytevectors)
        bytevector-length
        string->utf8 utf8->string)
  (only (rl3 env prelude)
        add1 sub1)
  (only (rl3 env parameters)
        parameterize)
  (only (rl3 io print)
        format)
  (only (rl3 types strings)
        string-index)
  (only (rl3 concurrency tasks)
        make-task task-join! task-start! end-tasking without-interrupts)
  (only (rl3 concurrency tasks-with-io)
        with-tasking-io
        input-not-ready-handler output-not-ready-handler)
  (only (rl3 io net sockets)
        server-socket server-socket-accept
        socket-descriptor socket-close
        socket-input-port socket-output-port)
  (only (rl3 web uri)
	http-path-path
	parse-http-path)
  (only (rl3 web uri path)
	path-split)
  (only (rl3 web http http)
	parse-http-start-line
	http-start-line-path
        http-header-from-socket-input-port)
  (only (rl3 web httpserver log)
	www-log)
  (only (rl3 web httpserver dispatch)
	rest-resource
	dispatch)
  (only (rl3 web httpserver utils)
        string-split))
 
 (define http-server-task
   (lambda (dispatch-tree port)
     (lambda ()
       (let ((s (server-socket port '())))
         ;;(socket/nonblock (socket-representation-sock s))
         (let accept-loop ()
           (let-values (((socket addr)
		       (server-socket-accept s '())))
		     (www-log "Accept: ~s ~s~%" socket addr)
		     (flush-output-port (current-output-port))
		     (task-start! (make-task (http-service-task dispatch-tree socket addr) "http"))
		     (accept-loop)))))))
 
 (define http-service-task
   (lambda (dispatch-tree socket addr)
     (lambda ()
       (let ((in-port  (socket-input-port socket))
	   (out-port (socket-output-port socket)))
;;         (www-log  "Got in port ~s~%" in-port)
         (let ((request (http-header-from-socket-input-port in-port)))
           (www-log "Request ~s~%" request)
	   (dispatch request in-port out-port dispatch-tree)
;;	   (www-log "Socket Close ~s.~%" socket)	   
	   (socket-close socket))))))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Main routine to launch the server. ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (define web-server 
   (lambda (configure port)
     (let ((dispatch-tree (configure)))
       (with-tasking-io
	(lambda ()
	  (let ((listener-task (make-task
			      (http-server-task dispatch-tree port)
			      "listener")))
	    (www-log "Starting server on port ~s.~%" port)	
	    (www-log "Dispatch Tree ~%")
	    (display dispatch-tree)(newline)
	    (task-start! listener-task)
	    (task-join!  listener-task)))))))
 ;;(repl)
 ;;(end-tasking)))))


 )
