(library 
 (rl3 web httpserver static)
 
 (export
  www-root-set!
  serve-static-resource
  static-resource)
 
 (import
  (rnrs base)
  (only (rnrs control)
	do unless)
  (only (rnrs io simple)
	eof-object?
        call-with-input-file)
  (only (rnrs io ports)
	open-file-input-port close-port
        get-line put-bytevector)
  (only (rnrs bytevectors)
        string->utf8)
  ;;  (only (rl3 io print)
  ;;	pretty-print)
  (only (rl3 env parameters)
        parameterize)
  (only (rl3 env larceny)
        error-handler)
  (only (rl3 io print)
	format)
  (only (rl3 web http http)
	http-send-response)
  (only (rl3 web httpserver log)
	www-log)
  (only (rl3 web httpserver utils)
	normalize-path)
  (only (rl3 text text)
	weave-string-separator)
  (only (rl3 web httpserver dispatch)
	rest-resource))
 
;; (define *www-root*    "/code/rl3/repo/bravais/static")
 
 (define *www-root* "/dev/null")

 (define *www-default* "index.html")	      ;; name to use if request ends in / 
 
 (define crlf
   (string->utf8 "\r\n"))

 ;; I am not happy about this.  FIXME RPR
 (define www-root-set!
   (lambda (path)
     (set! *www-root* path)))

 (define static-default-age
   (string-append "max-age=" (number->string (* 60 60 24)))) ;; one day

 (define serve-static-resource
   (lambda (path-segs output-port)
     (let ((path (normalize-path path-segs)))
       (if path
	   (let ((full-path(string-append *www-root* "/" (weave-string-separator "/" path))))
	     (www-log "GET - ~s~%" full-path)
	     (unless (call-with-current-continuation
		      (lambda (k)
			(parameterize ((error-handler
					(lambda e
					  (www-log "Unexpected exception ~s~%" e)
					  (www-log "ERROR - Failed to send static resource ~s ~%" full-path)
					  (k #f))))
				      (let ((ip (open-file-input-port full-path)))
					(http-send-response "200 OK" `(("Content-Type"  . "text/html; charset=UTF8")
								       ("Cache-Control" . ,static-default-age))
							    output-port ip 0)
					(close-port ip))
				      #t)))
		     (let ((bad-path (weave-string-separator "/" path)))
		       (www-log "BAD PATH - ~s ~%" bad-path)
		       (put-bytevector output-port (string->utf8 (string-append "Request for static resource failed!"))))))))))
 
 (define static-resource
   (rest-resource
    (GET (lambda (request remainder input-port output-port)
	   (let ((path-remainder (car remainder)))
	     (serve-static-resource path-remainder output-port))))))
 
 )
