
(import
 (rnrs base)
 (only (rnrs control)
       when)
 (rl3 env debug)
 (rl3 test unit-tests)
 (only (rl3 web uri)
       parse-http-path http-path-path http-path-query http-path-fragment)
 (rl3 web httpserver dispatch))

(import 
 (only (rl3 web http)
       parse-http-start-line
       http-start-line-method http-start-line-path http-start-line-version)
 (only (rl3 web uri path)
       path-split))

(debug-enable #f)

(begin 

  (define request 
    "GET /books/b/c/d.txt?type=paper HTTP/1.1")

  (define request2
    "GET /people/person/123456 HTTP/1.1")

  (define make-handler
    (lambda (sym)
      (lambda (request ip path-remainder)
	(cons sym path-remainder))))

  (define dispatch-tree
    `(("" ,(make-handler 'index)
       ("books"   ,(make-handler 'books)
	("book"   ,(make-handler 'book)))
       ("people"  ,(make-handler 'people)
	("person" ,(make-handler 'person))))))

  (define book-resource
    (rest-resource
     (GET (lambda (request input-port path-remainder)
	    (display path-remainder)
	    (newline)))
     (HEAD (lambda (request input-port path-remainder) 
	     'BHEAD))))

  (when (test-runner-current)
	(test-runner-reset (test-runner-current)))


  (dispatch-path-on-tree (path-split (http-path-path (parse-http-path (http-start-line-path (parse-http-start-line request)))))
			 dispatch-tree #f)


  (dispatch request  #f dispatch-tree)
  (dispatch request2 #f dispatch-tree)

  )
