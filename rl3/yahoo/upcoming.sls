#| Library for Yahoo's upcoming API |#

(library
 (rl3 yahoo upcoming)
 
(export
 event-search)

(import 
  (rnrs base)
  (rnrs io simple) ;; debug only
  (rnrs io ports)
  (only (rl3 io ascii-ports)
	make-ascii-input-port)
  (only (rl3 web uri url parms)
	parms->query)
  (only (rl3 web uri)
	make-uri uri->string)
  (only (rl3 web http http)
	http-invoke)
  (only (rl3 web http headers)
	host-header)
  (only (rl3 web html htmlprag)
	html->sxml)
  (only (rl3 yahoo credentials)
	yahoo-credentials-upcoming-key)
  (only (rl3 xml ssax ssax)
	xml->sxml))
  
 ;; Events
 (define event-get-info #f)
 (define event-add #f)
 (define event-edit #f)
 (define event-add-tags #f)
 (define event-remove-tag #f)
 
(define upcoming-host "upcoming.yahooapis.com")
(define upcoming-path "/services/rest/")

(define base-parms
    (let ((base #f))
      (lambda (creds)
	(if base 
	   base 
	   (begin (set! base `(("api_key" . ,(yahoo-credentials-upcoming-key creds)))) 
	      base)))))

 (define event-search 
   (lambda (creds search-text location-text)
     (let ((parms (append (base-parms creds) 
			`(("method"      . "event.search")
			  ("search_text" . ,search-text)
			  ("location"    . ,location-text)))))
       (let ((uri (make-uri "http" #f upcoming-host #f 
			  upcoming-path (parms->query parms) "")))
	 (let-values (((hdrs hip) (http-invoke 'GET uri `(,(host-header upcoming-host)))))
		   (display hdrs)
		   (newline)
		   (let ((tip (make-ascii-input-port hip)))
 		     (let ((results (html->sxml tip)))
 		       (close-port tip)
 		       results)))))))

 (define event-get-watch-list #f)
 (define event-get-groups #f)
      
 )

     ;; http://upcoming.yahooapis.com/services/rest/?api_key=<mykey>&method=event.search&search_text=engulfed+flames&metro_id=1