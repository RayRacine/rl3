(library
 (rl3 yahoo credentials)

 (export 
  yahoo-load-credentials
  yahoo-credentials-upcoming-key)

 (import 
  (rnrs base)
  (rnrs lists)
  (only (rnrs io simple)
	read call-with-input-file)
  (err5rs records syntactic))

 (define-record-type yahoo-credentials #t #t upcoming-key)
 
 (define yahoo-load-credentials
   (lambda (fpath)
     (call-with-input-file fpath
       (lambda (ip)
	 (let ((props (read ip)))
	   (make-yahoo-credentials (cdr (assoc 'upcoming-key props))))))))
 
)