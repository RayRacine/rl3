(library 
 (rl3 adt graph)

 (export
  graph-adj-lists graph-node-adj-list-by-id ;; debug
  graph-node-seek-adj-list-by-id

  graph make-graph graph? graph-size 
  graph-edges graph-nodes
  node-equal? 
  graph-add-node graph-add-edge
  graph-node graph-node-by-id
  graph-remove-node graph-remove-node-by-id
  node make-node node? node-id)
 
 (import
  (rnrs base)
  (rnrs lists)
  (rnrs conditions)
  (rnrs exceptions)
  (rnrs io simple)
  (err5rs records syntactic)
  (only (larceny records printer)
        rtd-printer-set!)
  (rl3 io print))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; A node simply has a unique identity and is capable of being added to a graph
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 (define-record-type node #t #t (id))

 ;; two job-nodes are equal if they have the same id
 (define node-equal?
   (lambda (node1 node2)
     (equal? (node-id node1) (node-id node2))))

 ;; define a singlton unique type 
 ;; signifies an unspecified value
 (define-record-type (null-node node) #t #t)
 
 (define node-null (make-null-node "*NODE-NULL*"))

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; A graph is a adj. list of nodes ;;
 ;; a -> (b,c) means edges exists
 ;; FROM a TO c and FROM a TO c.
 ;; i.e. directed graph
 ;; For a undirected graph use two edges
 ;; a-b and b-a.
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

 ;; graph type
 ;; name string? - name of the graph, not necessarily unique
 ;; nodes list? - listof a listof nodes where the first node is the src node, the remainder are the dest nodes
 ;; ((a b c) (b d) (c) (d)) is the graph with nodes a b c d and edges a->b, a->c b->d.

 (define-record-type graph 
   ctor-graph #t
   name adj-lists)
 
 (define make-graph
   (lambda (name)
     (ctor-graph name '())))
 
 (define graph-size
   (lambda (graph)
     (length (graph-adj-lists graph))))

 (define node-has-id?
   (lambda (node id)
     (equal? (node-id node) id)))

 ;; seek into the graph till we find the adj-list for the given node id
 ;; returns an pair where fst is the list where the first node is the sought adj-lst and
 ;; the remainder of hte list is the predeccesor nodes processed and snd is the adj-lst
 ;;
 ;; return
 ;; '() if node does not exists
 ;;  ( (node pred-node ...) . (succ-node ...))
 (define graph-node-seek-adj-list-by-id
   (lambda (graph id)
     (let loop ((adj-lists (graph-adj-lists graph)) (accum '()))
       (if (null? adj-lists)
	  (cons (list (list node-null)) accum)  ;; adj-lst #f means did not find, do this so don't lose the reversed accum
	  (let ((adj-lst (car adj-lists)))
	    (if (node-has-id? (car adj-lst) id)
	       (cons (cons adj-lst accum) (cdr adj-lists))
	       (loop (cdr adj-lists) (cons adj-lst accum))))))))

 (define graph-node-adj-list-by-id
   (lambda (graph id)
     (let ((node-succ-preds (graph-node-seek-adj-list-by-id graph id)))
       (let ((adj-lst (caar node-succ-preds)))
	 (if (eq? node-null (car adj-lst))
	    #f
	    adj-lst)))))
 
 ;; get a node from the graph which is node-equal? to the given node.
 (define graph-node
   (lambda (graph node)
     (graph-node-by-id
      (graph (node-id node)))))
 
 ;; get a node from a graph
 (define graph-node-by-id
   (lambda (graph id)
     (let ((adj-lst (graph-node-adj-list-by-id graph id)))
       (if adj-lst
	  (car adj-lst)
	  #f))))

 ;; Add a new node to a graph.
 ;; It is an error to add a preexisting node.
 ;; Otherwise add the node.
 (define graph-add-node
   (lambda (graph node)
     (if (node? node)
	(let ((id (node-id node)))
	  (let loop ((adj-lists (graph-adj-lists graph)))
	    (if (null? adj-lists)
	       (ctor-graph (graph-name graph) (cons (list node) (graph-adj-lists graph)))
	       (let ((adj-lst (car adj-lists)))
		 (if (node-has-id? (car adj-lst) id)
		    graph
		    (loop (cdr adj-lists))))))))))

 ;; add a node to the adj-lst
 ;; returns
 ;;  adj-lst already in adj-lst
 ;;  new adj-lst with node added
 (define add-node-to-adj-list
   (lambda (adj-lst node)
     (let loop ((to-nodes (cdr adj-lst)))
       (if (null? to-nodes)
	  (cons (car adj-lst) (cons node to-nodes))
	  (if (node-equal? (car adj-lst) node)
	     adj-lst
	     (loop (cdr to-nodes)))))))

 ;; given 2 lists of nodes append the first to the second.
 ;; Yea I know what you're thinking out there. :)
 (define node-append
   (lambda (hns tns)
     (pretty-print "======")
     (pretty-print hns)
     (pretty-print tns)
     (let loop ((hns hns) (tns tns))
       (if (null? hns)
	  tns
	  (loop (cdr hns) (cons (car hns) tns))))))

 ;; check and see if the from node exists
 ;; then add the to node to the from adj-lst
 (define graph-add-edge 
   (lambda (graph from-node to-node)
     (if (graph-node-adj-list-by-id graph (node-id to-node))
	(let ((succ-preds (graph-node-seek-adj-list-by-id graph (node-id from-node))))
	  (let ((succs (car succ-preds))
	      (preds (cdr succ-preds)))
	    (if (eq? node-null (caar succs))
	       (raise (condition (make-error)
				 (make-message-condition "Adding edge where from node does not exist in graph.")))
	       (ctor-graph (graph-name graph)
			   (node-append preds (cons (add-node-to-adj-list (car succs) to-node) (cdr succs)))))))
	(raise (condition (make-error) 
			  (make-message-condition "Adding edge where from node does not exist in graph."))))))
 
 ;;    (lambda (graph from-node to-node)
 ;;      (let ((adj-lst (graph-node-adj-list-by-id graph (node-id to-node))))
 ;;        (if (not (null-node? (car adj-lst)))
 ;; 	  (let ((adj-lst (graph-node-adj-list-by-id graph (node-id from-node))))
 ;; 	  (if (null-node? (car adj-lst))
 ;; 	     (raise (condition (make-error) 
 ;; 			       (make-message-condition "Adding edge where from node does not exist in graph.")))
 ;; 	     (ctor-graph (graph-name graph)
 ;; 			 (append (cdr adj-list)
 ;;    				   (cons (add-node-to-adj-list to-node) (
 ;;    	       (let ((adj-lst (car adj-lists)))
 ;;    		 (if (node-equal? (car adj-lst) from-node)
 ;;    		    (let ((adj-lst (add-node-to-adj-list adj-lst to-node)))
 ;;    		      (pretty-print adj-lst)
 ;;    		      (if adj-lst
 ;;    			 (ctor-graph (graph-name graph)
 ;;    				     (append (reverse accum) ;; yep, rev is not necessary. RPR
 ;;    					     (cons adj-lst (cdr adj-lists))))
 ;;    			 graph)) ;; edge already exists
 ;;    		    (loop (cdr adj-lists) (cons adj-lst accum))))))
 ;;   				   (raise (condition (make-error)
 ;;    			    (make-message-condition "Adding edge where to node does not exist in graph.")))))))))))
 
 (define graph-remove-node
   (lambda (graph node)
     (graph-remove-node graph (node-id node))))

 (define graph-remove-node-by-id
   (lambda (graph id)
     (let loop ((adj-lists (graph-adj-lists graph)) (accum '()))
       (if (null? adj-lists)
	  graph
	  (if (node-has-id? node (caar adj-lists))
	     (make-graph (graph-name graph) (append accum (cdr adj-lists)))
	     (loop (cdr adj-lists) (cons (car adj-lists) accum)))))))

 ;; return all edges as list of pair of nodes.
 ;;  (a,b) => is a directed edge from a to b.
 (define graph-edges
   (lambda (graph)
     (let loop ((adj-lists (graph-adj-lists graph)) (edges '()))
       (if (null? adj-lists)
	  edges
	  (let ((adj-lst (car adj-lists)))
	    (let ((es (let ((from-node (car adj-lst)))
		      (map (lambda (node)
			     (cons from-node node))
			   (cdr adj-lst)))))
	      (loop (cdr adj-lists) 
		    (if (null? es)
		       edges
		       (append es edges)))))))))

 ;; return a list containing all the nodes in the graph.
 (define graph-nodes
   (lambda (graph)
     (let loop ((adj-lists (graph-adj-lists graph)) (nodes '()))
       (if (null? adj-lists)
	  nodes
	  (loop (cdr adj-lists) (cons (caar adj-lists) nodes))))))

 (define to-string
   (lambda (value)
     (let ((os (open-output-string)))
       (display value os)
       (get-output-string os))))
 
 (rtd-printer-set! node
		   (lambda (node out)
		     (display "#<node \"" out)
		     (display (to-string (node-id node)) out)
		     (display "\">" out)))


 )

;; (library
;;  (rl3 adt digraph)

;;  (export)

;;  (import 
;;   (rnrs base))

;;  (define-record-type digraph
;;    ctor-digraph #t
;;    name (ins) (outs))

;;  (define make-digraph
;;    (lambda (name)
;;      (ctor-digraph name '() '())))

;;  (define digraph-size
;;    (lambda (digraph)
;;      (fx+ (length (digraph-ins  digraph))
;; 	  (length (digraph-outs digraph)))))

;;  (




