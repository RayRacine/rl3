;; CML trans-id

;; Port or Reppy's CML to Larceny Scheme.

;; Tasks are often blocked in any number of queues.
;; A trans-id is used to markk blocked tasks.
;; A trans-id has a state of CANCEL or TRANS.
;; The CANCEL state is signifies some other event was selected.

(library
 (rl3 concurrency trans-id)
 
 (export
  make-trans-id trans-id?  
  trans-id-state
  trans-id-cancel!
  make-flag force)
 
 (import 
  (rnrs base)
  (rnrs control)
  (rnrs exceptions)
  (rnrs conditions)
  (only (rnrs io simple)
	display)
  (err5rs records syntactic)
  (only (larceny records printer)
	rtd-printer-set!))
  
 ;; Note Note: is critical section required here???? RPR
 (define-record-type trans-id-rtd
   ctor-trans-id
   trans-id?
   ;; (one-of/c 'TRANS CANCEL)
   (state trans-id-state trans-id-state-set!))

 (define make-trans-id
   (lambda ()
     (ctor-trans-id 'TRANS)))

 (define trans-id-cancel!
   (lambda (trans-id)
     (trans-id-state-set! trans-id  'CANCEL)))
 
 ;; create a transaction flag which is a pair (txid . cleanup)
 ;; cleanup is a thunk which sets the txid state to CANCEL 
 (define make-flag
   (lambda ()
     (let ((txid (make-trans-id)))
       (cons txid (lambda ()
		    (trans-id-state-set! txid 'CANCEL))))))

 ;; Mark the the trans-id cancelled
 (define force
   (lambda (tx-id)
     (case (trans-id-state tx-id)
       ((TRANS)  (trans-id-state-set! tx-id 'CANCEL))
       (else (raise (condition (make-error)
			       (make-message-condition "Forcing a CANCELed trans-id.")))))))
 
 (rtd-printer-set! trans-id-rtd
		   (lambda (tx-id out)
		     (display "#<trans-id " out)
		     (display (symbol->string (trans-id-state tx-id)) out)
		     (display ">" out)))
 )

;;  (define-syntax critical-property
;;    (syntax-rules ()
;;      ((critical-property 'accessor rtd field)
;;       (let ((accessor (rtd-accessor rtd 'field)))
;; 	(lambda (rec)
;; 	  (accessor rec))))
;;      ((critical-property 'mutator rtd field)
;;       (let ((mutator (rtd-mutator rtd 'field)))
;; 	(lambda (rec val)
;; 	  (mutator rec val))))))
