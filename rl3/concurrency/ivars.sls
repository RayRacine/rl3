;; Simple implementation of a CML IVar via ConditionVariable.

;; IVars are memory cells which have have 2 states empty or full.
;; An attempt to read blocks the calling thread until a value is present.
;; Putting a value to a filled IVar results in a  Put Exception.


(library 
 (rl3 concurrency ivars)
 
 (export
  make-ivar
  ivar-put ivar-get)
 
 (import
  (rnrs base)
  (rnrs control)
  (rnrs exceptions)
  (rnrs conditions)
  (err5rs records syntactic)
  (only (rl3 env prelude)
	unspecified)
  (only (rl3 concurrency tasks)
	task-block
	current-task
	make-condition-variable
	condition-variable-put!
	condition-variable-broadcast!
	without-interrupts))

 (define-record-type ivar
   ctor-ivar #t (ready) cond-var (value))

 (define make-ivar
   (lambda ()
     (ctor-ivar #f (make-condition-variable "ivar") (unspecified))))

 (define ivar-get
   (lambda (ivar)
     (let loop () ;; avoid invoking scheduling/blocking as being in a critical section by re-checking ready 
       (if (ivar-ready ivar)
	  (ivar-value ivar)
	  (begin (condition-variable-put! (ivar-cond-var ivar) (current-task))
	     (task-block (current-task))
	     (loop))))))

 (define ivar-put
   (lambda (ivar value)
     (without-interrupts
      (when (ivar-ready ivar)
	    (raise (condition (make-error)
			      (make-message-condition "Attempted to put to an IVar with a value.")
			      (make-irritants-condition ivar))))
      (ivar-value-set! ivar value)
      (ivar-ready-set! ivar #t)
      (condition-variable-broadcast! (ivar-cond-var ivar)))))
   
 )
