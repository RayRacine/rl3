;;(library
 
;; (rl3 concurrency green-tasks mutexes)
 
;; (export)
 
;; (import)
;;   (primitives record-updater)
;;   (rnrs base)
;;   (only (rnrs io simple)
;;         display newline)
;;   (only (rnrs control)
;;         case-lambda when)
;;   (only (rnrs arithmetic fixnums)
;;         fxand fxzero?)
;;   (err5rs records procedural)
;;   (only (err5rs records inspection)
;;         record-rtd)
;;   (only (rl3 env prelude)
;;         unspecified)
;;   (only (rl3 concurrency tasks)
;;         current-task task-alive? task-block task-unblock without-interrupts)
;;   (only (rl3 concurrency green-tasks condition-variables)
;; 	condition-variable-put!))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;; SRFI-18 implemntation
;; ;;; Mutex
;; ;;; by Ray Racine
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; Borrows heavily from the orginal
;; ;; Larceny lib/Standard/mutex.sch


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;                     MUTEX States
;; ;;
;; ;; I don't know where the SRFI-18 author gets his concept naming from ...
;; ;; LOCKED-OWNED  = 0 0xb00 => Locked and owned by a task T
;; ;; NOT-OWNED     = 2 0xb10 => Locked without an owner.
;; ;; NOT-ABANDONED = 1 0xb01 => Not locked, not abandoned.
;; ;; ABANDONED     = 3 0xb11 => Not locked, abandoned.
;; ;; First bit determines locked / unlocked status.
;; ;; Second bit determines owend / abandoned status.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (define locked/owned           0)
;; (define locked/not-owned       2)
;; (define unlocked/not-abandoned 1)
;; (define unlocked/abandoned     3)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; Manually define the mutex record type
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (define mutex-rtd
;;   (make-rtd 'mutex '#(state         ;; fixnum, LOCKED NOT-OWNED ABANDONED NOT-ABANDONED
;; 		      owner         ;; Owning Task
;; 		      blocked       ;; blocked tasks
;; 		      name          ;; string, useful for debugging
;; 		      specific)))   ;; user specific slot

;; (define make-mutex
;;   (let* ((ctor (rtd-constructor mutex-rtd))
;;        (make (lambda (name)
;; 	       (ctor unlocked/not-abandoned
;; 		     #f '() name (unspecified)))))
;;     (case-lambda
;;      (() (make "anon"))
;;      ((name) (make name)))))

;; (define mutex?
;;   (rtd-predicate mutex-rtd))

;; (define mutex-name
;;   (rtd-accessor mutex-rtd 'name))

;; (define mutex-name-set!
;;   (rtd-mutator mutex-rtd 'name))

;; (define mutex-specific
;;   (rtd-accessor mutex-rtd 'specific))

;; (define mutex-specific-set!
;;   (rtd-mutator mutex-rtd 'specific))

;; (define state-bits
;;   (rtd-accessor mutex-rtd 'state))

;; (define state-bits-set!
;;   (rtd-mutator mutex-rtd 'state))

;; (define blocked
;;   (rtd-accessor mutex-rtd 'blocked))

;; (define blocked-set!
;;   (rtd-mutator mutex-rtd 'blocked))

;; (define owner
;;   (rtd-accessor mutex-rtd 'owner))

;; (define owner-set!
;;   (rtd-mutator mutex-rtd 'owner))

;; (define locked?
;;   (let ((lock-mask #b01))
;;     (lambda (mutex)
;;       (fxzero? (fxand (state-bits mutex) lock-mask)))))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; mutex-state
;; ;;
;; ;;Returns information about the state of the mutex. The possible results are:
;; ;;  * task T: the mutex is in the locked/owned state and
;; ;;    task T is the owner of the mutex
;; ;;  * symbol NOT-OWNED: the mutex is in the locked/not-owned state
;; ;;  * symbol ABANDONED: the mutex is in the unlocked/abandoned state
;; ;;  * symbol NOT-ABANDONED: the mutex is in the unlocked/not-abandoned state 
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (define mutex-state
;;   (lambda (mutex)
;;     (if (locked? mutex)
;; 	(owner mutex)
;; 	(case (state-bits mutex)
;; 	  ((1) 'NOT-ABANDONED)
;; 	  ((2) 'NOT-OWNED)
;; 	  ((3) 'ABANDONED)))))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; (mutex-lock! mutex [timeout [task]])
;; ;;
;; ;; If the mutex is currently locked, the current task waits until the mutex is unlocked,
;; ;; or until the timeout is reached if timeout is supplied.
;; ;;
;; ;; If the timeout is reached, mutex-lock! returns #f.
;; ;; Otherwise, the state of the mutex is changed as follows:
;; ;;
;; ;;  * if task is #f the mutex becomes locked/not-owned,
;; ;;  * otherwise, let T be task (or the current task if task is not supplied),
;; ;;      o if T is terminated the mutex becomes unlocked/abandoned,
;; ;;      o otherwise mutex becomes locked/owned with T as the owner. 
;; ;;
;; ;; After changing the state of the mutex, an "abandoned mutex exception" is raised if the
;; ;; mutex was unlocked/abandoned before the state change, otherwise mutex-lock! returns #t.
;; ;;
;; ;; It is not an error if the mutex is owned by the current task
;; ;; (but the current task will have to wait).
;; ;;
;; ;; Notes: 1) Timeouts not implmented, yet, and are ignored.
;; ;;        2) abandoned mutex exception is not thrown.
;; ;;        3) Original Larceny impl was reentrant for the same task with a count.
;; ;;           SRFI-18 says a reentrant task will block on a lock it already owns.
;; ;;           Why?  If no reason reveals itself, lets put back reentrant locks.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (define mutex-lock!
;;   (let ((lock
;; 	 (lambda (mutex timeout task)  ;; timeout ignored
;; 	   (without-interrupts     ;; how can we be looping without interrupts PRR ???  prob.  double lock pattern here.      
;; 	    (let loop ()
;; 	      (newline)
;; 	      (if (locked? mutex)
;; 		  (begin           ;; wait
;; 		    (blocked-set! mutex (cons task (blocked mutex)))
;; 		    (task-block task)  ;; block -> removes from scheduler, and yields (switched).
;; 		    (loop))
;; 		  (if (not task) ;; lock
;; 		      (state-bits-set! mutex locked/not-owned)
;; 		      (if (task-alive? task)
;; 			  (begin
;; 			    (state-bits-set! mutex locked/owned)
;; 			    (owner-set! mutex task))
;; 			  (state-bits-set! mutex locked/not-owned)))) ;; dead task, srfi-18 requires exception here
;; 	      #t)))))
    
;;     (case-lambda    
;;      ((mutex)
;;       (lock mutex -1 (current-task)))
;;      ((mutex timeout)         ;; timeout ignored
;;       (lock mutex -1 (current-task)))
;;      ((mutex timeout task)
;;       (lock mutex timeout task)))))

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; (mutex-unlock! mutex [condition-variable [timeout]])  ;procedure
;; ;;
;; ;; Unlocks the mutex by making it unlocked/not-abandoned.
;; ;; It is not an error to unlock an unlocked mutex and a mutex that is owned by any task.
;; ;;
;; ;; If condition-variable is supplied, the current task is blocked and added to the condition-variable
;; ;; before unlocking mutex; the task can unblock at any time but no later than when an appropriate call
;; ;; to condition-variable-signal! or condition-variable-broadcast! is performed (see below), and no later
;; ;; than the timeout (if timeout is supplied).
;; ;;
;; ;; If there are tasks waiting to lock this mutex, the scheduler selects a task, 
;; ;; the mutex becomes locked/owned or locked/not-owned, and the task is unblocked.
;; ;; mutex-unlock! returns #f when the timeout is reached, otherwise it returns #t.
;; ;;
;; ;; Notes: 1) Timeout is currently ignored and assumes a value of infinite, ie, wait forever.
;; ;;        2) The above SRFI-18 condition-variable logic demands a strict sequence, which we do not do.
;; ;;           However, all steps are atomic so we meet the semantics.
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (define mutex-unlock!
;;   (let ((unlock
;; 	 (lambda (mutex condition-variable timeout)
;; 	   (without-interrupts
;; 	    (state-bits-set! mutex unlocked/not-abandoned)                
;; 	    (owner-set! mutex #f)
;; 	    (let ((blocked (blocked mutex)))
;; 	      (blocked-set! mutex '())
;; 	      (for-each task-unblock blocked))
;; 	    (when condition-variable
;; 		  (let ((task (current-task)))
;; 		    (condition-variable-put! condition-variable task)
;; 		    (task-block task)))      ;; block -> removes from scheduling, yields (task:switch)
;; 	    #t))))
    
;;     (case-lambda
;;      ((mutex)
;;       (unlock mutex #f -1))
;;      ((mutex condition-variable)
;;       (unlock mutex condition-variable -1))
;;      ((mutex condition-variable timeout)
;;       (unlock mutex condition-variable timeout)))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; define a printer routine for when a mutex
;; ;; is displayed
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;   ((record-updater (record-rtd mutex-rtd) 'printer)
;; ;;    mutex-rtd
;; ;;    (lambda (mutex out) 
;; ;;      (display "<mutex \"" out)
;; ;;      (display (string-append (mutex-name mutex) ":"
;; ;;                              (symbol->string (mutex-state mutex)) ":"
;; ;;                              (number->string (state-bits mutex))) out)
;; ;;      (display "\">" out)))

;; )