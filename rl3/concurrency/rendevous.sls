

;; ;; Rendezvous combinators

;; ;; There's considerable potential for more abstraction in the protocol
;; ;; for primitive rendezvous. --Mike

;;  (library
;;   (rl3 concurrency rendevous)

;;   (export)
  
;;   (import 
;;    (rnrs base)
;;    (rnrs control)
;;    (rnrs arithmetic fixnums)
;;    (err5rs records procedural)
;;    (err5rs records syntactic)
;;    (only (rl3 env prelude)
;; 	 unspecified)
;;    (only (rl3 concurrency tasks primitives)
;; 	 disable-interrupts enable-interrupts)
;;    (only (rl3 types lambda)
;; 	 identity)
;;    (only (rl3 adt queue)
;; 	 queue-put!)
;;    (only (rl3 concurrency trans-id)
;; 	 trans-id-cancel!)
;;    (only (rl3 concurrency tasks)
;; 	 task-unblock))
  
;; ;;  (only (rl3 concurrency trans-id)


;;  ;; execute critical section code without interrupts
;;  ;; Q is this truly safe without dyncamic-wind protection?? RPR
;;  (define-syntax critical-section
;;    (syntax-rules ()
;;      ((critical-section e0 e1 ...)
;;       (let ((tics (disable-interrupts)))
;; 	(let ((r (begin e0 e1 ...)))
;; 	  (when tics (enable-interrupts tics))
;; 	  r)))))

;; ;;   datatype 'a status =
;; ;;          ENABLED of {prio : int, doitFn : unit -> 'a}
;; ;;        | BLOCKED of {transId : trans_id,
;; ;;                      cleanUp : unit -> unit,
;; ;;                      next : unit -> rdy_thread} -> 'a


;;  ;; Define possible values of a base event thunk.
;;  ;; event-status? => (or/c enabled-status? blocked-status?) 

;;  (define-record-type status-rtd #f status?)
 
;;  (define-record-type (enabled-status-rtd status-rtd)
;;    make-enabled-status
;;    enabled-status?
;;    (priority enabled-status-priority)
;;    ;; doit-proc is a thunk, (-> any)
;;    (doit-proc enabled-status-doit-proc)) 

;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;  ;; PROC is a procedure with three arguments:
;;  ;; a TRANS-ID, a CLEANUP-PROC, and a WRAP-PROC.
;;  ;;
;;  ;; TRANS-ID is the transaction ID of the blocked thread. 
;;  ;;
;;  ;; CLEANUP-PROC is a procedure which takes a thread queue as an
;;  ;; argument; it can enqueue thread cells whose threads it wants
;;  ;; woken up.
;;  ;;
;;  ;; WRAP-PROC is the complete, composed-together chain of WRAP
;;  ;; procedures of the event.
;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
;;  (define-record-type (blocked-status-rtd status-rtd)
;;    make-blocked-status
;;    blocked-status?
;;    ;; proc : (-> trans-id? (-> void?) (-> ready-thread?) any)
;;    (proc blocked-proc))

;;  ;;   type 'a base = unit -> 'a status 
;;  ;;   datatype 'a event =
;;  ;;          BEVT of 'a base list
;;  ;;        | CHOOSE of 'a event list
;;  ;;        | GUARD of unit -> 'a event
;;  ;;        | WNACK of unit event -> 'a event
 
;;  (define-record-type event-rtd #f event?)
 
;;  ;; a base event is a list of polling thunks
;;  ;; which returns an status record
;;  ;; event-status? => (or/c enabled-status? blocked-status?)
;;  (define-record-type (base-event-rtd event-rtd)
;;    ctor-base
;;    base-event?
;;    ;; (listof/c  (-> event-status?))
;;    (poll-thunks base-event-poll-thunks))
 
;;  (define (make-base poll-thunk)
;;    (ctor-base (list poll-thunk)))

;;  (define-record-type (choose-rtd event-rtd)
;;    make-choose
;;    choose?
;;    ;; (list-of event?)
;;    (events choose-events))

;;  (define-record-type (guard-rtd event-rtd)
;;    make-guard
;;    guard?
;;    ;; (-> event?)
;;    (thunk guard-thunk))

;;  (define-record-type (with-nack-rtd event-rtd)
;;    make-with-nack
;;    with-nack?
;;    ;; (-> event? event?)
;;    (proc with-nack-proc))

;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ;; Condition variables for internal use to implement this library.
;;  ;; cvars allow threads to perform condition synchronization
;;  ;;   It allows threads to block, waiting for a specified condition associated 
;;  ;;   with a condition variable—to occur, and have other threads to wake up 
;;  ;;   the waiting threads when the condition is fulfilled.
;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;  ;; A cvar is a cell with one of two state values, cvar-set or cvar-unset
;;  ;; a set cvar has a state value of integer?
;;  ;; an unset cvar has a state values of a (listof/c cvar-item?)
;;  (define-record-type cvar-rtd
;;    ctor-cvar
;;    cvar?
;;    (state cvar-state cvar-state-set!))

;;  ;; init to an unset state of empty items
;;  (define (make-cvar)
;;    (ctor-cvar '()))

;;  (define make-cvar-set-state
;;    (lambda (priority)
;;      (ctor-cvar priority)))

;;  (define cvar-set?
;;    (lambda (cvar)
;;      (fixnum? (cvar-state cvar))))

;;  (define cvar-unset?
;;    (lambda (cvar)
;;      (let ((state (cvar-state cvar)))
;;        (cond
;; 	((list? state)	 
;; 	 (or (null? state)
;; 	    (cvar-item? (car state))))
;; 	(else #f)))))

;;  (define cvar-set-priority
;;    (lambda (cvar)
;;      (cvar-state cvar)))

;;  (define cvar-unset-items
;;    (lambda (cvar)
;;      (cvar-state cvar)))

;;  ;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ;; cvar-item record type
;;  ;; for unset state
;;  ;;;;;;;;;;;;;;;;;;;;;;;;;

;;  (define-record-type cvar-item-rtd
;;    make-cvar-item
;;    cvar-item?
;;    ;; trans-id?
;;    (trans-id cvar-item-trans-id cvar-item-trans-id-set!)
;;    ;; cleanup thunk (-> unit)
;;    (cleanup cvar-item-cleanup)
;;    ;; task?
;;    (task cvar-item-task))
 
;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ;; Condition variables.  Because these variables are set inside atomic
;;  ;; regions, we have to use different conventions for clean-up, etc.
;;  ;; Instead of requiring the blockFn continuation to call the cleanUp
;;  ;; action and to leave the atomic region, we call the cleanUp function
;;  ;; when setting the condition variable (in atomicCVarSet), and have the
;;  ;; invariant that the blockFn continuation is dispatched outside the
;;  ;; atomic region.
;;  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ;;
;;  ;; !!! ATOMIC !!!
;;  ;;
;;  (define (cvar-set! cvar)
;;    (let ((state (cvar-state cvar))
;;        ;; perfomance short cut - if not a fixnum, cvar-set? assume its a cvar-unset?
;;        (set? (lambda (state)
;; 	       (fixnum? state))))
;;      (cond
;;       ((not (set? state)) ;; is unset
;;        ;; ready waiting tasks
;;        (for-each (lambda (cvar-item)
;; 		   (let ((trans-id (cvar-item-trans-id cvar-item)))
;; 		     (case trans-id
;; 		       ((TRANS)
;; 			(cvar-item-trans-id-set! cvar-item 'CANCEL)
;; 			((cvar-item-cleanup cvar-item))
;; 			;; WHAT SHOULD I REALL DO HERE???? RPR
;; 			(task-unblock (cvar-item-task cvar-item)))))) ;; how to I ready???
;; 		 state)
;;        ;; now set it
;;        (cvar-state-set! cvar (make-cvar-set-state 1)))
;;       (else
;;        (assertion-violation 'cvar-set! "cvar already set")))))
;; )

;; ;; START HERE RPR

;; (define (cvar-get-rv cvar)
;;   (make-base
;;    (lambda ()
;;      (let ((state (cvar-state cvar)))
;;        (cond
;; 	((cvar-set-state? state)
;; 	 (let ((priority (cvar-set-state-priority state)))
;; 	   (cvar-set-state-priority-set! state (fx+ 1 priority))
;; 	   (make-enabled priority
;; 			 (lambda (queue)
;; 			   (cvar-set-state-priority-set! state 1)
;; 			   (unspecified)))))
;; 	(else
;; 	 (make-blocked
;; 	  (lambda (trans-id cleanup-proc wrap-proc)
;; 	    (cvar-unset-state-blocked-set!
;; 	     state
;; 	     (cons (make-cvar-item trans-id cleanup-proc wrap-proc)
;; 		   (cvar-unset-state-blocked state)))))))))))


;; (define (always-rv value)
;;   (make-base
;;    (lambda ()
;;      (make-enabled -1
;; 		   (lambda (queue)
;; 		     value)))))

;; (define never-rv (ctor-base '()))

;; (define (guard thunk)
;;   (ctor-guard thunk))

;; (define (with-nack proc)
;;   (make-nack-rtd proc))

;; (define (gather-prim-rvs rev-rvs prim-rvs)
;;   (cond
;;    ((null? rev-rvs) (ctor-base prim-rvs))
;;    ((not (base? (car rev-rvs)))
;;     (if (null? prim-rvs)
;;        (gather rev-rvs '())
;;        (gather rev-rvs (list (ctor-base prim-rvs)))))
;;    ;; (car rev-rvs) is base
;;    (else
;;     (gather-prim-rvs (cdr rev-rvs)
;; 		     (append (base-prim-rvs (car rev-rvs))
;; 			     prim-rvs)))))

;; (define (gather rev-rvs rvs)
;;   (cond
;;    ((not (null? rev-rvs))
;;     (let ((rv (car rev-rvs)))
;;       (cond
;;        ((choose? rv)
;; 	(gather (cdr rev-rvs) (append (choose-rvs rv) rvs)))
;;        ((and (base? rv)
;; 	   (not (null? rvs))
;; 	   (base? (car rvs)))
;; 	(gather (cdr rev-rvs)
;; 		(cons (ctor-base (append (base-prim-rvs rv)
;; 						(base-prim-rvs (car rvs))))
;; 		      (cdr rvs))))
;;        (else
;; 	(gather (cdr rev-rvs) (cons rv rvs))))))
;;    ((null? (cdr rvs)) (car rvs))
;;    (else (make-choose rvs))))

;; (define (choose . rvs)
;;   (gather-prim-rvs (reverse rvs)  '()))

;; (define (compose f g)
;;   (lambda (x)
;;     (f (g x))))

;; (define (wrap-prim-rv prim-rv wrap-proc)
;;   (really-make-prim-rv (compose wrap-proc 
;; 				(prim-rv-wrap-proc prim-rv))
;; 		       (prim-rv-poll-thunk prim-rv)))

;; (define (wrap rv wrap-proc)
;;   (cond
;;    ((base? rv)
;;     (really-make-base (map (lambda (prim-rv)
;; 			     (wrap-prim-rv prim-rv wrap-proc))
;; 			   (base-prim-rvs rv))))
;;    ((choose? rv)
;;     (make-choose (map (lambda (rv)
;; 			(wrap rv wrap-proc))
;; 		      (choose-rvs rv))))
;;    ((guard? rv)
;;     (make-guard (lambda ()
;; 		  (wrap ((guard-thunk rv)) wrap-proc))))
;;    ((nack? rv)
;;     (make-nack (lambda (nack-rv)
;; 		 (wrap ((nack-proc rv) nack-rv) wrap-proc))))))

;; (define-record-type base-group-rtd
;;   ctor-base-group
;;   base-group?
;;   (prim-rvs base-group-prim-rvs))

;; ;; (define-record-discloser :base-group
;; ;;   (lambda (base-group)
;; ;;     (cons 'base-group 
;; ;; 	  (base-group-prim-rvs base-group))))

;; (define-record-type choose-group-rtd
;;   make-choose-group
;;   choose-group?
;;   (groups choose-group-groups))

;; ;; (define-record-discloser :choose-group
;; ;;   (lambda (choose-group)
;; ;;     (cons 'choose-group 
;; ;; 	  (choose-group-groups choose-group))))

;; (define-record-type nack-group-rtd
;;   make-nack-group
;;   nack-group?
;;   (cvar nack-group-cvar)
;;   (group nack-group-group))

;; ;; (define-record-discloser :nack-group
;; ;;   (lambda (nack-group)
;; ;;     (list 'nack-group
;; ;; 	  (nack-group-group nack-group))))

;; (define (force-rv rv)
;;   (cond
;;    ((base? rv) 
;;     (really-make-base-group (base-prim-rvs rv)))
;;    (else
;;     (really-force-rv rv))))

;; (define (force-prim-rvs rvs prim-rvs)
;;   (if (null? rvs)
;;      (really-make-base-group prim-rvs)
;;      (let* ((rv (car rvs))
;; 	  (group (really-force-rv rv)))
;;        (cond
;; 	((base-group? group)
;; 	 (force-prim-rvs (cdr rvs)
;; 			 (append (base-group-prim-rvs group)
;; 				 prim-rvs)))
;; 	((choose-group? group)
;; 	 (force-rvs (cdr rvs)
;; 		    (append (choose-group-groups group)
;; 			    (list (really-make-base-group prim-rvs)))))
;; 	(else
;; 	 (force-rvs (cdr rvs)
;; 		    (list group (really-make-base-group prim-rvs))))))))

;; (define (force-rvs rvs groups)
;;   (cond
;;    ((not (null? rvs))
;;     (let* ((rv (car rvs))
;; 	 (group (really-force-rv rv)))
;;       (cond
;;        ((and (base-group? group)
;; 	   (not (null? groups))
;; 	   (base-group? (car groups)))
;; 	(force-rvs (cdr rvs)
;; 		   (cons (really-make-base-group
;; 			  (append (base-group-prim-rvs group)
;; 				  (base-group-prim-rvs (car groups))))
;; 			 (cdr groups))))
;;        ((choose-group? group)
;; 	(force-rvs (cdr rvs)
;; 		   (append (choose-group-groups group)
;; 			   groups)))
;;        (else
;; 	(force-rvs (cdr rvs) (cons group groups))))))
;;    ((null? (cdr groups))
;;     (car groups))
;;    (else
;;     (make-choose-group groups))))

;; ;; this corresponds to force' in Reppy's implementation

;; (define (really-force-rv rv)
;;   (cond
;;    ((guard? rv)
;;     (really-force-rv ((guard-thunk rv))))
;;    ((nack? rv)
;;     (let ((cvar (make-cvar)))
;;       (make-nack-group cvar
;; 		       (really-force-rv
;; 			((nack-proc rv)
;; 			 (cvar-get-rv cvar))))))
;;    ((base? rv)
;;     (really-make-base-group (base-prim-rvs rv)))
;;    ((choose? rv)
;;     (force-prim-rvs (choose-rvs rv) '()))
;;    (else
;;     (assertion-violation 'really-force-rv "not a rendezvous" rv))))

;; (define (sync-prim-rv prim-rv)
;;   (let ((poll-thunk (prim-rv-poll-thunk prim-rv))
;;       (wrap-proc (prim-rv-wrap-proc prim-rv)))
;;     (let ((status (poll-thunk)))
;;       (cond
;;        ((enabled? status)
;; 	(let* ((queue (make-queue))
;; 	     (value ((enabled-do-proc status) queue)))
;; 	  (wrap-proc value)))
;;        ((blocked? status)
;; 	(let ((trans-id (make-trans-id)))
;; 	  ((blocked-proc status) trans-id values wrap-proc)
;; 	  (cond
;; 	   ((maybe-commit-and-trans-id-value trans-id)
;; 	    => (lambda (pair)
;; 		 (set-current-proposal! old)
;; 		 ((cdr pair) (car pair)))))))))))


;; (define (select-do-proc priority+do-list n)
;;   (cond
;;    ((null? (cdr priority+do-list))
;;     (cdar priority+do-list))
;;    (else
;;     (let ((priority
;; 	 (lambda (p)
;; 	   (if (= p -1)
;; 	      n
;; 	      p))))
;;       (let max ((rest priority+do-list)
;; 	      (maximum 0)
;; 	      (k 0) ; (length do-procs)
;; 	      (do-list '())) ; #### list of pairs do-proc * wrap-proc
;; 	(cond
;; 	 ((not (null? rest))
;; 	  (let* ((pair (car rest))
;; 	       (p (priority (car pair)))
;; 	       (stuff (cdr pair)))
;; 	    (cond 
;; 	     ((> p maximum)
;; 	      (max (cdr rest) p 1 (list stuff)))
;; 	     ((= p maximum)
;; 	      (max (cdr rest) maximum (+ 1 k) (cons stuff do-list)))
;; 	     (else
;; 	      (max (cdr rest) maximum k do-list)))))
;; 	 ((null? (cdr do-list))
;; 	  (car do-list))
;; 	 (else
;; 	  ;; List.nth(doFns, random k)
;; 	  (car do-list))))))))

;; (define (block)
;;   (with-new-proposal (lose)
;; 		     (maybe-commit-and-block (make-cell (current-thread)))))

;; (define (sync-prim-rvs prim-rvs)
;;   (cond
;;    ((null? prim-rvs)
;;     (block))
;;    ((null? (cdr prim-rvs)) (sync-prim-rv (car prim-rvs)))
;;    (else
;;     (let ((old (current-proposal)))
;;       (let lose ()
;; 	(define (find-enabled prim-rvs block-procs wrap-procs)
;; 	  (if (null? prim-rvs)
;; 	     (let ((trans-id (make-trans-id)))
;; 	       (for-each (lambda (block-proc wrap-proc)
;; 			   (block-proc trans-id values wrap-proc))
;; 			 block-procs wrap-procs)
;; 	       (cond
;; 		((maybe-commit-and-trans-id-value trans-id)
;; 		 => (lambda (pair)
;; 		      (set-current-proposal! old)
;; 		      ((cdr pair) (car pair))))
;; 		(else
;; 		 (lose))))
;; 	     (let* ((prim-rv (car prim-rvs))
;; 		  (poll-thunk (prim-rv-poll-thunk prim-rv))
;; 		  (wrap-proc (prim-rv-wrap-proc prim-rv))
;; 		  (status (poll-thunk)))
;; 	       (cond
;; 		((enabled? status)
;; 		 (handle-enabled (cdr prim-rvs)
;; 				 (list
;; 				  (cons (enabled-priority status)
;; 					(cons (enabled-do-proc status)
;; 					      wrap-proc)))
;; 				 1))
;; 		((blocked? status)
;; 		 (find-enabled (cdr prim-rvs)
;; 			       (cons (blocked-proc status)
;; 				     block-procs)
;; 			       (cons wrap-proc wrap-procs)))))))

;; 	(define (handle-enabled prim-rvs priority+do-list priority)
;; 	  (if (null? prim-rvs)
;; 	     (let* ((stuff (select-do-proc priority+do-list priority))
;; 		  (do-proc (car stuff))
;; 		  (wrap-proc (cdr stuff))
;; 		  (queue (make-queue))
;; 		  (value (do-proc queue)))
;; 	       (if (maybe-commit-and-make-ready queue)
;; 		  (begin
;; 		    (set-current-proposal! old)
;; 		    (wrap-proc value))
;; 		  (lose)))
;; 	     (let* ((prim-rv (car prim-rvs))
;; 		  (poll-thunk (prim-rv-poll-thunk prim-rv))
;; 		  (wrap-proc (prim-rv-wrap-proc prim-rv))
;; 		  (status (poll-thunk)))
;; 	       (cond
;; 		((enabled? status)
;; 		 (handle-enabled (cdr prim-rvs)
;; 				 (cons (cons (enabled-priority status)
;; 					     (cons (enabled-do-proc status)
;; 						   wrap-proc))
;; 				       priority+do-list)
;; 				 (+ 1 priority)))
;; 		(else
;; 		 (handle-enabled (cdr prim-rvs)
;; 				 priority+do-list
;; 				 priority))))))

;; 	(set-current-proposal! (make-proposal))

;; 	(find-enabled prim-rvs '() '()))))))

;; (define (sync rv)
;;   (let ((group (force-rv rv)))
;;     (cond
;;      ((base-group? group)
;;       (sync-prim-rvs (base-group-prim-rvs group)))
;;      (else
;;       (sync-group group)))))

;; (define-record-type ack-flag-rtd
;;   ctor-ack-flag 
;;   ack-flag?
;;   (acked flag-acked? ack-flag-acked-set!))

;; (define (make-ack-flag)
;;   (ctor-ack-flag #f))

;; (define (ack-flag! ack-flag)
;;   (set-flag-acked?! ack-flag #t))

;; (define-record-type flag-set-rtd
;;   make-flag-set
;;   flag-set?
;;   (cvar flag-set-cvar)
;;   (ack-flags flag-set-ack-flags))

;; (define (check-cvars! flag-sets queue)
;;   (for-each (lambda (flag-set)
;; 	      (check-cvar! flag-set queue))
;; 	    flag-sets))

;; (define (check-cvar! flag-set queue)
;;   (let loop ((ack-flags (flag-set-ack-flags flag-set)))
;;     (cond
;;      ((null? ack-flags)
;;       (cvar-set! (flag-set-cvar flag-set) queue))
;;      ((flag-acked? (car ack-flags))
;;       (values))
;;      (else
;;       (loop (cdr ack-flags))))))

;; ;; this corresponds to syncOnGrp from Reppy's code
;; (define (sync-group group)
;;   (call-with-values
;;       (lambda () (collect-group group))
;;     really-sync-group))

;; ;; This is analogous to SYNC-PRIM-RVS

;; (define (really-sync-group prim-rv+ack-flag-list flag-sets)
  
;;   (let ((old (current-proposal)))
    
;;     (let lose ()
;;       (define (find-enabled prim-rv+ack-flag-list
;; 		       block-proc+ack-flag-list
;; 		       wrap-procs)
;; 	(if (null? prim-rv+ack-flag-list)
;; 	   (let ((trans-id (make-trans-id)))
;; 	     (for-each (lambda (block-proc+ack-flag wrap-proc)
;; 			 (let ((block-proc (car block-proc+ack-flag))
;; 			     (ack-flag (cdr block-proc+ack-flag)))
;; 			   (block-proc trans-id
;; 				       (lambda (queue)
;; 					 (ack-flag! ack-flag)
;; 					 (check-cvars! flag-sets queue))
;; 				       wrap-proc)))
;; 		       block-proc+ack-flag-list wrap-procs)
;; 	     (cond
;; 	      ((maybe-commit-and-trans-id-value trans-id)
;; 	       => (lambda (pair)
;; 		    (set-current-proposal! old)
;; 		    ((cdr pair) (car pair))))
;; 	      (else
;; 	       (lose))))
;; 	   (let* ((prim-rv (caar prim-rv+ack-flag-list))
;; 		(ack-flag (cdar prim-rv+ack-flag-list))
;; 		(poll-thunk (prim-rv-poll-thunk prim-rv))
;; 		(wrap-proc (prim-rv-wrap-proc prim-rv))
;; 		(status (poll-thunk)))
;; 	     (cond
;; 	      ((enabled? status)
;; 	       (handle-enabled (cdr prim-rv+ack-flag-list)
;; 			       (list
;; 				(cons (enabled-priority status)
;; 				      (cons (cons (enabled-do-proc status) ack-flag)
;; 					    wrap-proc)))
;; 			       1))
;; 	      ((blocked? status)
;; 	       (find-enabled (cdr prim-rv+ack-flag-list)
;; 			     (cons (cons (blocked-proc status) ack-flag)
;; 				   block-proc+ack-flag-list)
;; 			     (cons wrap-proc wrap-procs)))))))

;;       (define (handle-enabled prim-rv+ack-flag-list priority+do-list priority)
;; 	(if (null? prim-rv+ack-flag-list)
;; 	   (let* ((stuff (select-do-proc priority+do-list priority))
;; 		(more-stuff (car stuff))
;; 		(do-proc (car more-stuff))
;; 		(ack-flag (cdr more-stuff))
;; 		(wrap-proc (cdr stuff))
;; 		(queue (make-queue)))
;; 	     (ack-flag! ack-flag)
;; 	     (check-cvars! flag-sets queue)
;; 	     (let ((value (do-proc queue)))
;; 	       (if (maybe-commit-and-make-ready queue)
;; 		  (begin
;; 		    (set-current-proposal! old)
;; 		    (wrap-proc value))
;; 		  (lose))))
;; 	   (let* ((prim-rv+ack-flag (car prim-rv+ack-flag-list))
;; 		(prim-rv (car prim-rv+ack-flag))
;; 		(ack-flag (cdr prim-rv+ack-flag))
;; 		(poll-thunk (prim-rv-poll-thunk prim-rv))
;; 		(wrap-proc (prim-rv-wrap-proc prim-rv))
;; 		(status (poll-thunk)))
;; 	     (cond
;; 	      ((enabled? status)
;; 	       (handle-enabled (cdr prim-rv+ack-flag-list)
;; 			       (cons (cons (enabled-priority status)
;; 					   (cons (cons (enabled-do-proc status) ack-flag)
;; 						 wrap-proc))
;; 				     priority+do-list)
;; 			       (+ 1 priority)))
;; 	      (else
;; 	       (handle-enabled (cdr prim-rv+ack-flag-list)
;; 			       priority+do-list
;; 			       priority))))))

;;       (set-current-proposal! (make-proposal))

;;       (find-enabled prim-rv+ack-flag-list '() '()))))

;; (define (collect-group group)
;;   (cond
;;    ((choose-group? group)
;;     (gather-choose-group group))
;;    (else
;;     (gather-wrapped group '() '()))))

;; (define (gather-choose-group group)
;;   (let ((ack-flag (make-ack-flag)))
;;     (let gather ((group group)
;; 	       (prim-rv+ack-flag-list '())
;; 	       (flag-sets '()))
;;       (cond
;;        ((base-group? group)
;; 	(let append ((prim-rvs (base-group-prim-rvs group))
;; 		   (prim-rv+ack-flag-list prim-rv+ack-flag-list))
;; 	  (if (null? prim-rvs)
;; 	     (values prim-rv+ack-flag-list flag-sets)
;; 	     (append (cdr prim-rvs)
;; 		     (cons (cons (car prim-rvs) ack-flag)
;; 			   prim-rv+ack-flag-list)))))
;;        ((choose-group? group)
;; 	;; fold-left
;; 	(let loop ((groups (choose-group-groups group))
;; 		 (prim-rv+ack-flag-list prim-rv+ack-flag-list)
;; 		 (flag-sets flag-sets))
;; 	  (if (null? groups)
;; 	     (values prim-rv+ack-flag-list flag-sets)
;; 	     (call-with-values
;; 		 (lambda ()
;; 		   (gather (car groups)
;; 			   prim-rv+ack-flag-list
;; 			   flag-sets))
;; 	       (lambda (prim-rv+ack-flag-list flag-sets)
;; 		 (loop (cdr groups)
;; 		       prim-rv+ack-flag-list
;; 		       flag-sets))))))
;;        ((nack-group? group)
;; 	(gather-wrapped group prim-rv+ack-flag-list flag-sets))))))

;; (define (gather-wrapped group prim-rv+ack-flag-list flag-sets)
;;   (call-with-values
;;       (lambda ()
;; 	(let gather ((group group)
;; 		   (prim-rv+ack-flag-list prim-rv+ack-flag-list)
;; 		   (all-flags '())
;; 		   (flag-sets flag-sets))
;; 	  (cond
;; 	   ((base-group? group)
;; 	    (let append ((prim-rvs (base-group-prim-rvs group))
;; 		       (prim-rv+ack-flag-list prim-rv+ack-flag-list)
;; 		       (all-flags all-flags))
;; 	      (if (null? prim-rvs)
;; 		 (values prim-rv+ack-flag-list
;; 			 all-flags
;; 			 flag-sets)
;; 		 (let ((ack-flag (make-ack-flag)))
;; 		   (append (cdr prim-rvs)
;; 			   (cons (cons (car prim-rvs) ack-flag)
;; 				 prim-rv+ack-flag-list)
;; 			   (cons ack-flag all-flags))))))
;; 	   ((choose-group? group)
;; 	    ;; fold-left
;; 	    (let loop ((groups (choose-group-groups group))
;; 		     (prim-rv+ack-flag-list prim-rv+ack-flag-list)
;; 		     (all-flags all-flags)
;; 		     (flag-sets flag-sets))
;; 	      (if (null? groups)
;; 		 (values prim-rv+ack-flag-list
;; 			 all-flags
;; 			 flag-sets)
;; 		 (call-with-values
;; 		     (lambda ()
;; 		       (gather (car groups)
;; 			       prim-rv+ack-flag-list
;; 			       all-flags
;; 			       flag-sets))
;; 		   (lambda (prim-rv+ack-flag-list all-flags flag-sets)
;; 		     (loop (cdr groups)
;; 			   prim-rv+ack-flag-list all-flags flag-sets))))))
;; 	   ((nack-group? group)
;; 	    (call-with-values
;; 		(lambda ()
;; 		  (gather (nack-group-group group)
;; 			  prim-rv+ack-flag-list
;; 			  '()
;; 			  flag-sets))
;; 	      (lambda (prim-rv+ack-flag-list all-flags-new flag-sets)
;; 		(values prim-rv+ack-flag-list
;; 			(append all-flags-new all-flags)
;; 			(cons (make-flag-set (nack-group-cvar group)
;; 					     all-flags-new)
;; 			      flag-sets))))))))
;;     (lambda (prim-rv+ack-flag-list all-flags flag-sets)
;;       (values prim-rv+ack-flag-list flag-sets))))


;; (define (select . rvs)
;;   (sync (apply choose rvs)))
;; )
