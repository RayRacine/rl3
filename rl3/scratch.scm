;;; SXML Test
(import (rnrs base)
	(rnrs io simple)
	(rnrs io ports)
	(rl3 env debug))

(debug-enable #f)

(import 
 (rl3 xml ssax ssax)
 (rl3 xml sxml sxpath)
 (rl3 xml sxml serializer))

;;(call-with-input-file "/code/rl3/repo/rl3/rl3/xml/ssax/tiny.xml"
(call-with-input-file "/home/raymond/isbnservice.wso"
  (lambda (ip)
    (let ((ns '((w . "http://schemas.xmlsoap.org/wsdl/") 
	      (tns . "http://webservices.daehosting.com/ISBN")
	      (s . "http://schemas.xmlsoap.org/wsdl/soap/"))))
      (pretty-print (xml->sxml ip ns)))))


;;; HTTP Server

(import (rnrs base)
        (rl3 env debug))

(debug-enable #f)

(import (rl3 web httpserver server))

;;(web-server 8080)

;;; Simple Amazon A2S calls

(import (rnrs base)
        (rnrs io simple)
        (rl3 io print)
        (rl3 aws a2s a2s)
        (rl3 aws awscredentials)
	(rl3 xml sxml sxpath)
        (rl3 concurrency tasks-with-io))

(define creds (load-credentials "/home/raymond/awsaccount.txt"))

(with-tasking-io
 (lambda ()
   (let ((ns '((a2s . "http://webservices.amazon.com/AWSECommerceService/2008-04-07"))))
     (let ((search-result (keyword-search creds "pickles")))
       (pretty-print (extract-result (car ((sxpath "/a2s:ItemSearchResponse/a2s:Items/a2s:Item" ns)
					   search-result))))))))

;;(display (pretty-print (keyword-search creds "pickles")))))

(with-tasking-io
 (lambda ()
   (let ((ns '((a2s . "http://webservices.amazon.com/AWSECommerceService/2008-03-03"))))
     (display (pretty-print (item-lookup creds "B000V78UWM"))))))
     ;;(display ((sxpath "/a2s:ItemLookupResponse/a2s:OperationRequest/a2s:RequestId" ns) (item-lookup creds "B000V78UWM"))))))


;;; Simple Amazon S3 list buckets

(import (rnrs base)
        (rnrs io simple)
        (rl3 io print)
        (rl3 aws s3 s3)
        (rl3 aws awscredentials)
        (rl3 concurrency tasks-with-io))

(define creds (load-credentials "/home/ray/awsaccount.txt"))

(with-tasking-io
 (lambda ()
   (display (pretty-print (list-buckets creds)))))

;;;
;;;
;;;

(import (rnrs base)
        ;;(rnrs r5rs)
        ;;(rnrs lists)
        (rnrs bytevector)s
        (rnrs io simple)
        (rnrs io ports)
        (rnrs unicode)
        (rnrs mutable-pairs)
        ;;(rnrs mutable-strings)
        (rl3 concurrency tasks-with-io)
        (rl3 env debug)
        (rl3 io net sockets)
        (rl3 io print)
        (rl3 web http)
        (rl3 web pipes htmlprag)
        (primitives time))

(let ()
  (debug-enable #t)
  (with-tasking-io
   (lambda ()
     (let ((surl "GET /home/will/Larceny/ HTTP/1.1\r\nHost: www.ccs.neu.edu\r\nUser-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.5) Gecko/20070718 Fedora/2.0.0.5-1.fc7 Firefox/2.0.0.5\r\nAccept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\nAccept-Language: en-us,en;q=0.5\r\Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n\r\n")
	 (s (client-socket "www.ccs.neu.edu" 80)))
       (let ((op (socket-output-port s))
	   (ip (socket-input-port s)))
         (put-bytevector op (string->utf8 surl))
         (flush-output-port op)
         (let ((req (http-request ip)))
           (let ((hip (http-req-input-port req)))
             (let ((tip (http-ascii-port-from-binary-port hip)))
               (format #t "Header: ~a Len: ~a~%" (http-req-start-line req) (http-req-size req))
               (display (html->sxml tip))))))))))


(define run
  (lambda (t1 t2)
    (lambda ()
      (let ((t1 (make-thread t1 "t1"))
	  (t2 (make-thread t2 "t2")))
	(thread-start! t1)
	(thread-start! t2)
	(thread-join! t1)
	(thread-join! t2)
	(let loop ((n 10))
	  (if (zero? n)
	     'done))))))

(define athread
  (lambda ()
    (let loop ((n 100))
      (if (<= n 0)
	 (begin
	   (newline)
	   (display "********DONE**********")
	   (newline)
	   (flush-output-port)
	   'done)
	 (begin
	   (display n)
	   (display ", ")
	   (flush-output-port)
	   ;;(thread-yield)
	   (loop
	    (- n 1)))))))

;; (define t1 t2)

(import (rnrs))
(import (rnrs io simple))
(import (rnrs io ports))
(import (rl3 io net sockets))
(import (rl3 io net ipaddress))
(import (rl3 concurrency tasks))
(import (rl3 concurrency tasks-with-io))

(define gurl "GET / HTTP/1.1\r\nHost: www.google.com\r\nUser-Agent: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1.5) Gecko/20070718 Fedora/2.0.0.5-1.fc7 Firefox/2.0.0.5\r\nAccept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\nAccept-Language: en-us,en;q=0.5\r\Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\nKeep-Alive: 300\r\nConnection: keep-alive\r\n\r\n")

(with-tasking-io
 (lambda ()
   (let ((t (make-task
	   (lambda ()
	     (let ()
	       (define s (client-socket "www.google.com" 80))
	       
	       (define op (socket-output-port s))
	       (define ip (socket-input-port s))
	       
	       (put-bytevector op (string->utf8 gurl))
	       (flush-output-port op)
	       
	       (let loop ((b (get-bytevector-n ip 1)))
		 (if (or (eof-object? b)
		       (zero? (bytevector-length b)))
		    (socket-close s)
		    (begin
		      (display (utf8->string b))
		      (newline)
		      (flush-output-port (current-output-port))
		      (loop (get-bytevector-n ip 1)))))))
	   "worker")))
     (thread-start! t)
     (thread-join! t))))



(let ()
  (define s (client-socket "www.google.com" 80))
  
  (define op (socket-output-port s #t))
  (define ip (socket-input-port s #t))
  
  (put-bytevector op (string->utf8 gurl))
  (flush-output-port op)
  
  (let loop ((b (get-bytevector-n ip 1)))
    (if (or (eof-object? b)
	  (zero? (bytevector-length b)))
       (socket-close s)
       (begin
	 (display (utf8->string b))
	 (newline)
	 (flush-output-port (current-output-port))
	 (loop (get-bytevector-n ip 1))))))


(socket-error s)
(socket-close s)
(socket-descriptor s)
(define s #f)

(library
 (test)
 
 (export doit)
 
 (import
  (rnrs)
  (rnrs lists)
  (rnrs io simple)
  (sys system unix)
  (sys system process))
 
 (define reduce-r (lambda (pred l)
	       (let ((l1 (cdr l)))
		 (if (null? l1)
		    (car l)
		    (pred (car l) (reduce-r pred l1))))))

 (define (call-with-input-pipe command pred)
   (let* ((results (process (if (string? command)
                               command
                               (reduce-r (lambda (arg result)
                                           (string-append arg " " result))
                                         command))))
	  (result (pred (car results))))
     (close-input-port (car results))
     (close-output-port (cadr results))
     (unix-waitpid (caddr results)) ; important in order to remove process
     result))

 (define doit
   (lambda ()
     (call-with-input-pipe '("date" "-I")
			   (lambda (ip)
			     (utf8->string (get-bytevector-n ip 512))))))
 )

;; START HERE - OK must track length of HTTP header. Content-Length + Header = Total to read.

(let ((buffsz 32))
  (get-bytevector-n! (open-bytevector-input-port (make-bytevector 0))
                     (make-bytevector 32 0)
                     0 16))

(get-bytevector-n (open-bytevector-input-port (make-bytevector 0)) 16)






(haip (http-ascii-port-from-binary-port hip))
(html (html->sxml haip)))
(display html)
'done)))


;;       (if (chunked-encoding? header)
;;           (let loop ((chunk (get-chunk ip)))
;;             (if (eof-object? chunk)
;;                 (socket-close s)
;;                 (begin
;;                   (display "CHUNK -------------")
;;                   (newline)
;;                   ;;(display chunk)
;;                   ;;(newline)
;;                   (loop (get-chunk ip)))))))))


;;      (newline)
;;      (time (display (html->sxml ip)))
;;      (socket-close s))))

