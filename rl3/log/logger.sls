#| Port of David Rush's log system to R6RS |#

#| Currenlty compiles but port is not complete.
   DO NOT USE YET RPR June-1-2008
|#

(library
 (rl3 log logger)
 
 (export
  make-logger)
 
 (import
  (rnrs base)
  (rnrs lists)
  (rnrs io simple)
  (rnrs io ports)
  (err5rs records procedural)
  (only (rnrs r5rs)
	modulo)  
  (only (rl3 ffi ffi-std)
	foreign-procedure)
  (for (only (rl3 ffi foreign-ctools)
	     define-c-info)
       run expand)
  (only (rl3 types dates)
	current-time-utc)
  (only (rl3 io print)
	pretty-print)
  (primitives 
   find-tail
   current-utc-time
   receive
   cars+ans
   cdrs
   %cars+cdrs+
   make-continuation-inspector
   current-continuation-structure
   decode-error
   procedure-name
   error-handler
   exit
   with-output-to-string
   call-with-input-string
   call-with-output-string
   get-output-string
   open-output-string))  ;; FIXME RPR


 #| Define logger record type. |#
 (define logger-rtd
   (make-rtd 'logger '#(name pred semaphore grab release))) 
 
 (define make-logger
   (rtd-constructor logger-rtd))
 
 (define logger?
   (rtd-predicate logger-rtd))
 
 (define logger-semaphore
   (rtd-accessor logger-rtd 'semaphore))
 
 (define logger-name
   (rtd-accessor logger-rtd 'name))
 
 
 (define logger-pred
   (rtd-accessor logger-rtd 'pred))
 
 
 (define logger-grab 
   (rtd-accessor logger-rtd 'grab))
 
 
 (define logger-release 
   (rtd-accessor logger-rtd 'release))


#| 
Use a library global list of active loggers.
Routines used add and remove loggers.
|#
 
 ;; Library global list of active loggers
 (define *logs* '( () ))

 ;; A new logger to the library global list of loggers
 (define (log-add! log)
   (set! *logs* (cons log *logs*))
   log)
  
 (define log-remove-named-k! 
   (lambda (log-name k-success k-fail)
     (log-find-named-k log-name
		       (lambda (checked log unchecked)
			 (set! *logs* (append checked unchecked))
			 (k-success log))
		       k-fail)))

 (define log-remove-named! 
   (lambda (log-name)
     (log-remove-named-k! log-name 
			  (lambda (log*) *logs*) 
			  (lambda (log*) *logs*))))

 (define (log-find-named-k log-name k-success k-fail)
   (let find ((logger *logs*) (checked '(())))
     (if (null? logger)
	(k-fail log-name)
	(let ((log (car logger)) (unchecked (cdr log)))
	  (if (equal? log-name (logger-name log))
	     (k-success (reverse checked) log unchecked)
	     (find unchecked (cons log checked)))))))

 (define (log-remove! log)
   (log-remove-named! (logger-name log)))

 
 (define add-new-file-logger!
   (lambda (name pred? filename)
     (log-add! (make-port-logger name pred? 
				 (make-semaphore 1) 
				 (open-output-file filename)))))
 
 (define add-new-port-logger! 
   (lambda (name pred? port)
     (log-add! (make-port-logger name pred? (make-semaphore 1) port))))

 (define add-new-buffer-logger! 
   (lambda (name pred? size)
     (log-add! (make-buffer-logger name pred? (make-semaphore 1) 0 (make-vector size)))))

 ;; From (SRFI-1 . module-bigloo)
 (define fold 
   (lambda (kons knil lis1 . lists)
     (if (pair? lists)
	(let loop ((lists (cons lis1 lists)) (ans knil))
	  (receive (cars+ans cdrs) 
		   (%cars+cdrs+ lists ans)
		   (if (null? cars+ans)
		      ans
		      (loop cdrs (apply kons cars+ans)))))
	(let loop ((lis lis1) (ans knil))
	  (if (null-list? lis)
	     ans
	     (loop (cdr lis) (kons (car lis) ans)))))))


 (define (alist-map-assocs fn alist)
   (map (lambda (a) 
	  (fn (car a) (cdr a))) 
	alist))

 (define null-list? null?)

;;  (define (filter pred? list)
;;    (let cata
;;        ((l list) (ans '( ())))
;;      (if (null? l)
;; 	(reverse ans)
;; 	(let ((check (car l)))
;; 	  (if (pred? check)
;; 	     (cata (cdr l) (cons check ans))
;; 	     (cata (cdr l) ans))))))


;;  (define (remove pred? list)
;;    (filter (lambda (e) (not (pred? e))) list))


 (define batch/last-chance-handler 
   (lambda (puts)
     (lambda e
       (define (display-line s)
	 (puts (with-output-to-string (lambda () (write s)))))
       (display-line (quasiquote (lastchance error handler (unquote e))))
       (let* ((error-text
	     (call-with-output-string
	      (lambda (p) (decode-error e p))))
	    (stacktrace (current-continuation-structure))
	    (inspector (make-continuation-inspector stacktrace))
	    (summarize-frame
	     (lambda (count inspector . prefix)
	       (let* ((frame (inspector '( get)))
		    (code (frame '( code)))
		    (class (code '( class)))
		    (expr (code '( expression)))
		    (proc (code '( procedure))))
		 (display-line (quasiquote
				(frame (unquote-splicing prefix)
				       (unquote class)
				       (unquote-splicing
					(case class
					  ((system-procedure) '( ()))
					  ((interpreted-primitive)
					   (procedure-name proc))
					  ((interpreted-expression) expr)
					  ((compiled-procedure)
					   (procedure-name proc))
					  (else '( ()))))))))))
	    (backtrace
	     (lambda (count inspector)
	       (let loop ((c (inspector '( clone))))
		 (let ((f (c '(get))))
		   (if (f '(same?) (inspector '( get)))
		      (summarize-frame 0 c "=> ")
		      (summarize-frame 0 c "   ")))
		 (if (c '(down)) (loop c))))))
	 (display-line
	  (quasiquote (decoded error (unquote error-text))))
	 (backtrace 0 inspector)
	 (exit 0)))))
 
 
 (define (install-lastchance puts)
   (error-handler (batch/last-chance-handler puts)))
 

 #| Critical section semaphore routines for threaded logging |#

 (define (make-semaphore n) #t)

 (define semaphore-mutex (make-semaphore 0))

 (define (semaphore-wait mutex) #t)

 (define (semaphore-post sem) #t)
   

 (define (log-grab log)
   (semaphore-wait (logger-semaphore log))
   ((logger-grab log)))

 
 (define (log-release log)
   ((logger-release log))
   (semaphore-post (logger-semaphore log)))
  

 #| Log predicate combinators |#
 
 ;; Log anything predicate
 (define log-all 
   (lambda x #t))
 
 ;; Log nothing predicate
 (define log-none
   (lambda x #f))

 (define log-has-any?  
   (lambda s
     (cond ((null? s) log-none)
	   ((null? (cdr s))
	    (let ((match (car s)))
	      (lambda (c)
		(or (and (pair? c) (member match c))
		   (equal? match c)))))
	   (else (lambda (criteria)
		   (if (pair? criteria)
		      (let check ((vals s))
			(cond ((null? vals) #f)
			      ((begin (member (car vals) criteria)) #t)
			      (else (check (cdr vals)))))
		      (and (member criteria s))))))))
 
 (define log-has? 
   (lambda s
     (cond ((null? s) log-all)
	   ((null? (cdr s))
	    (let ((match (car s)))
	      (lambda (c)
		(or (and (pair? c) (member match c))
		   (equal? match c)))))
	   (else (lambda (criteria)
		   (and (pair? criteria)
		      (let check ((vals s))
			(cond ((null? vals) #t)
			      ((member (car vals) criteria)
			       (check (cdr vals)))
			      (else #f)))))))))

 (define log-fractal? 
   (lambda (number-order . v)
     (cond ((null? v) log-all)
	   ((null? (cdr v))
	    (let ((match (car v)))
	      (if (number? match)
		 (lambda (c)
		   (cond ((pair? c)
			  (let ((level (find number? c)))
			    (and level (number-order level match))))
			 ((number? c) (number-order c match))
			 (else #f)))
		 (lambda (c)
		   (if (pair? c)
		      (find (lambda (e) (equal? e match)) c)
		      (equal? c match))))))
	   (else (lambda (criteria)
		   (and (pair? criteria)
		      (let check ((v v) (c criteria))
			(cond ((null? v) #t)
			      ((null? criteria) #f)
			      (else (let ((target (car v)))
				      (if (number? target)
					 (check (cdr v)
						(find-tail (lambda (e)
							     (and (number? e)
								(>= number-order
								   target)))
							   c))
					 (check (cdr v)
						(find-tail (lambda (e)
							     (equal? target e))
							   c)))))))))))))

 (define log-and 
   (lambda log-preds
     (lambda (c)
       (let check ((preds log-preds))
	 (cond ((null? preds) #t)
	       (((car preds) c) (check (cdr preds)))
	       (else #f))))))


 (define log-or 
   (lambda log-preds
     (lambda (c)
     (let check ((preds log-preds))
       (cond ((null? preds) #f)
	     (((car preds) c) #t)
	     (else (check (cdr preds))))))))
 
 
 (define log-not 
   (lambda (pred?)
     (lambda (c) 
       (not (pred? c)))))

 ;; this should init the log system 
 (define log-default
   (add-new-buffer-logger! '(DEFAULT) log-all 1000))


 (define (log-relevant? log criteria)
   (assert (logger? log))
   ((logger-pred log) criteria))


 (define (relevant-logs s)
   (fold (lambda (log use)
	   (assert (logger? log))
	   (if (log-relevant? log s) 
	      (cons log use) use))
	 '(())
	 *logs*))

 (define (logit facility s)
   (for-each (lambda (log)
	       (let ((port (log-grab log)))
		 (write (quasiquote ((unquote facility) (unquote s)))
			port)
		 (newline port)
		 (log-release log)))
	     (relevant-logs facility)))


 (define (ppit facility s)
   (for-each
    (lambda (log)
      (let ((port (log-grab log)))
	(pretty-print
	 (quasiquote ((unquote facility) (unquote s)))
	 port)
	(log-release log)))
    (relevant-logs facility)))

					; From (FLog . module-bigloo)
 (define (ftimestamp)
   (call-with-values current-utc-time
     (lambda (secs fracs)
       (let ((fracs* (number->string fracs)))
	 (call-with-output-string
	  (lambda
	      (p)
	    (display secs p)
	    (display #\. p)
	    (display
	     (make-string (- 6 (string-length fracs*)) #\0)
	     p)
	    (display fracs* p)))))))
 

 (define *flog-use-current-output-port* #f)


 (define *flog-use-current-error-port* #f)


 (define
   (make-flog file-name)
   (lambda
       (s)
     (let ((entry (call-with-output-string
                   (lambda
		       (p)
                     (display
		      (quasiquote
		       ((unquote (ftimestamp)) (unquote-splicing s)))
		      p)
                     (newline p))))
	   (f ((foreign-procedure
                "fopen"
                '( (string string))
                '( void*))
	       file-name
	       "a")))
       ((foreign-procedure
	 "fputs"
	 '( (string void*))
	 '( int))
	entry
	f)
       ((foreign-procedure
	 "fclose"
	 '( (void*))
	 '( void))
	f)
       (if *flog-use-current-output-port*
	  (display entry (current-output-port)))
       (if *flog-use-current-error-port*
	  (display entry (current-error-port))))))


 (define flog (make-flog "fcgi.log"))


 (define logger-file! 
   (lambda (name pred? file)
     (log-add! (make-file-logger name pred? (make-semaphore 1) file))))

#| Various types of loggers 
A buffer logger logs into a vector ring of string ports (?).
A port logger logs to a output given at logger construction.
A file logger logs into file.
|#

 (define make-buffer-logger
   (lambda (name pred? sem current entries)
     (make-logger name pred? sem
		     (lambda ()
		       (let ((port (open-output-string)))
			 (vector-set! entries current port)
			 port))
		     (lambda ()
		       (let ((port (vector-ref entries current)))
			 (if (output-port? port)
			    (vector-set! entries current (get-output-string port)))
			 (set! current
			    (modulo (+ 1 current) (vector-length entries))))))))

 (define make-port-logger
   (lambda (name pred? sem port)
     (make-logger name pred? sem (lambda () port) (lambda () #t))))

 
 (define make-file-logger 
   (lambda (name pred? sem file)
     (let ((port #f) (flogger (make-flog file)))
       (make-logger name pred? sem
		    (lambda ()
		      (if (not port)
			 (begin (set! port (open-output-string)) port))
		      port)
		    (lambda ()
		      (if port
			 (let* ((s (get-output-string port))
			      (sexp (call-with-input-string s read)))
			   (flogger sexp)
			 (set! port #f))))))))



 (logit '(LOGGER)
	'(loaded logging utils))
 
 )
