(library 
 (rl3 control)

 (export aif)

 (import 
  (for (rnrs base) run expand)
  (for (rnrs syntax-case) run expand))
       
 (define-syntax aif 
   (lambda (stx) 
     (syntax-case stx () 
       ((aif condition consequent alternative) 
        (with-syntax ((it (datum->syntax (syntax aif) 'it))) 
		     (syntax 
		      (let ((val condition)) 
			(if val 
			   (let ((it val)) 
			     consequent) 
			   alternative))))))))

 )
