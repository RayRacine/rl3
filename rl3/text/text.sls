;; general string / text manipulation

(library
 (rl3 text text)
 
 (export 
  weave-string-separator
  for-lines)
 
 (import
  (rnrs base)
  (rnrs syntax-case)
  (only (rl3 types lists)
        weave))
 
 ;; weave a separator string strictly between every pair of strings in a list
 ;; (weave '("1" "2" "3") ",") -> "1,2,3"
 (define weave-string-separator
   (lambda (sep lst)
     (apply string-append (weave sep lst))))
 
 ;; given a file name open and process a file 
 ;; one line at a time
 (define-syntax for-lines
   (syntax-rules (in)
     ((_ line in fname body ...)
      (with-input-from-file fname
	(lambda ()
	  (let loop ((line (get-line (current-input-port))))
	    (if (eof-object? line) (void)
		(begin 
		  body ... 
		  (loop (get-line (current-input-port))))))))))) 
)
