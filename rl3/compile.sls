(library
 (rl3 compile)
 
 (export rl3-compile)

 (import 
  (rnrs base)
  (only (rnrs io simple)
	newline display)
  (larceny compiler)
  (primitives current-directory))

 (define rl3-base 
   "/code/rl3/repo/rl3")

 (define rl3-compile
   (lambda ()
     (current-directory rl3-base)
     (display (current-directory))
     (newline)
     (compile-stale-libraries)))
 )
