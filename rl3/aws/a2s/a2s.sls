(library
 (rl3 aws a2s a2s)
 
 (export keyword-search
	 item-lookup similarity-lookup	 
	 cart-create cart-clear cart-get cart-add)
 
 (import
  (rnrs base)
  (rnrs control)
  (rnrs io ports)
  (rnrs conditions)
  (rnrs exceptions)
  (only (rl3 env prelude)
	fx1+)
  (only (rl3 text text)
	weave-string-separator)
  (only (rl3 aws configuration)
	a2s-host)
  (only (rl3 aws awscredentials)
	aws-credentials-associate-tag
	aws-credentials-secret-key
	aws-credentials-access-key)
  (only (rl3 web uri)	
	make-uri uri->start-line-path-string
	url-encode-string)
  (only (rl3 web http http)
	http-invoke)
  (only (rl3 io ascii-ports)
	make-ascii-input-port)
  (only (rl3 xml ssax ssax)
	xml->sxml)
  (only (rl3 types dates)
	current-time-rfc2822)
  (only (rl3 aws awsauth)
	aws-s3-auth-str
	aws-s3-auth-mac)
  (only (rl3 web http headers)
	host-header date-header)
  (only (rl3 web uri url parms)
	parms->query)
  (only (rl3 web http headers)
	host-header)
  (only (rl3 aws configuration)
	a2s-ns)
  (only (rl3 web http resource)
	fetch-resource-xml)
  (rl3 io print))

 (define empty-response
   (lambda () '()))

 (define a2s-host-header (host-header a2s-host))

 (define search-parms
   '(("Operation"     . "ItemSearch")
     ;; ("SearchIndex"   . "Books")
     ("ResponseGroup" . "SalesRank,Small,EditorialReview,Images")))

 (define itemlookup-parms
   '(("Operation"   . "ItemLookup")))

 (define ecs-parms
   (lambda (creds)
     `(("Service"        . "AWSECommerceService")
       ("Version"        . "2008-06-26")         
       ("AssociateTag"   . ,(aws-credentials-associate-tag creds))
       ("AWSAccessKeyId" . ,(aws-credentials-access-key creds)))))

 (define keyword-search
   (lambda (creds index-sym words) 
     (let ((parms (append search-parms (ecs-parms creds)
			`(("Keywords" . ,(url-encode-string (open-string-input-port words) #t))
			  ,(case index-sym
			     ((KINDLE)
			      '("SearchIndex" . "KindleStore"))
			     (else '("SearchIndex" . "Books")))))))
       (let ((uri (make-uri "http" #f a2s-host #f "/onca/xml" (parms->query parms) "")))
	 (let-values (((hdrs hip) (http-invoke 'GET uri `(,a2s-host-header))))
		   (let ((tip (transcoded-port hip (make-transcoder (utf-8-codec)))))
		     (call/cc (lambda (k)
				(with-exception-handler
				 (lambda (ec)
				   (pretty-print "ERROR in item search.")
				   (pretty-print uri)
				   (pretty-print hdrs)				   
				   (close-port tip)
				   (k '()))
				 (lambda ()
				   (let ((results (xml->sxml tip '())))
				     (close-port tip)
				     results)))))))))))

 (define similarity-lookup  
   (lambda (creds asins)
     (let ((parms (append itemlookup-parms (ecs-parms creds)
			`(("IdType" . "ASIN")
			  ("ItemId" . ,(weave-string-separator "," asins))
			  ("ResponseGroup" . "SalesRank,ItemAttributes,Images,EditorialReview")))))
       ;;(pretty-print parms)
       (let ((uri (make-uri "http" #f a2s-host #f "/onca/xml" (parms->query parms) "")))
	 (fetch-resource-xml uri `(,a2s-host-header) empty-response)))))

 (define item-lookup
   (lambda (creds asin)
     (let ((parms (append itemlookup-parms (ecs-parms creds)
			`(("IdType" . "ASIN")
			  ("ItemId" . ,asin)
			  ("ResponseGroup" . "SalesRank,Small,ItemAttributes,EditorialReview,Images,Reviews,Offers,Similarities")))))
       (let ((uri (make-uri "http" #f a2s-host #f "/onca/xml" (parms->query parms) "")))
	 (let-values (((hdrs hip) (http-invoke 'GET uri `(,a2s-host-header))))
		   (let ((tip (transcoded-port hip (make-transcoder (utf-8-codec)))))
		     (call/cc (lambda (k)
				(with-exception-handler
				 (lambda (ec)
				   (pretty-print "ERROR in item lookup.")
				   (pretty-print uri)
				   (pretty-print hdrs)				   
				   (k '()))
				 (lambda ()
				   (let ((results (xml->sxml tip '())))
				     (close-port tip)
				     results)))))))))))

 ;; Generic call procedure to the REST A2S API 
 (define a2s-invoke
   (lambda (creds op-parms query-extra)
     (let ((parms (append op-parms (ecs-parms creds))))
       (let ((uri (make-uri "http" #f a2s-host #f "/onca/xml" 
			  (let ((parms (parms->query parms)))
			    (if query-extra
			       (string-append parms "&" query-extra)
			       parms))
			  "")))
	 (let-values (((hdrs hip) (http-invoke 'GET uri `(,a2s-host-header))))
		   (let ((tip (make-ascii-input-port hip)))
		     (let ((results (xml->sxml tip '())))
		       (close-port tip)
		       results)))))))

 #|  Cart API |#

 ;; START HERE -- TEST CART CREATE

 ;;  (define cart-modify #f)

 ;; Given a line number and an alist of (asin . qty) create a cart line
 ;; Item.1.ASIN=123&Item.1.Quantity=10
 (define make-cart-lines
   (lambda (entries)
     (let ((make-line (lambda (n line)
		      (let ((line-no (number->string n)))
			(string-append "Item."   line-no ".ASIN=" (car line)
				       "&Item."  line-no ".Quantity=" (number->string (cdr line)))))))
       (do ((accum '() (cons (make-line n (car entries)) accum))
	    (entries entries (cdr entries))
	    (n 1 (fx1+ n)))
	   ((null? entries) (weave-string-separator "&" (reverse accum)))))))


 ;; lines : alist of (item . qty)
 (define cart-create 
   (lambda (creds items)
     (let ((parms `(("Operation" . "CartCreate"))))
       (a2s-invoke creds parms (make-cart-lines items)))))

 (define cart-parms
   (lambda (operation cart-id hmac-encoded)
     `(("Operation" . ,operation)
       ("CartId"    . ,cart-id)
       ("HMAC"      . ,hmac-encoded))))

 (define cart-clear
   (lambda (creds cart-id hmac-encoded)
     (a2s-invoke creds (cart-parms "CartClear" cart-id hmac-encoded) #f)))
 
 (define cart-get
   (lambda (creds cart-id hmac-encoded)
     (a2s-invoke creds (cart-parms "CartGet" cart-id hmac-encoded) #f)))

 (define cart-add
   (lambda (creds cart-id hmac-encoded items)
     (a2s-invoke creds 
		 (cart-parms "CartAdd" cart-id hmac-encoded)
		 (make-cart-lines items))))

 )
