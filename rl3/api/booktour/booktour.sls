(library
 (rl3 api booktour booktour)
 
 (export 
  booktour-search)
 
 (import 
  (rnrs base)
  (rnrs io simple) ;; debug only
  (rnrs io ports)
  (only (rl3 io ascii-ports)
	make-ascii-input-port)
  (only (rl3 web uri url parms)
	parms->query)
  (only (rl3 web uri)
	make-uri uri->string)
  (only (rl3 web http http)
	parse-http-response-line
	response-line-code
	http-invoke)
  (only (rl3 web http headers)
	host-header)
  (only (rl3 web html htmlprag)
	html->sxml)
  (only (rl3 xml ssax ssax)
	xml->sxml))

 (define booktour-host "booktour.com")
 (define booktour-path "/readers/rss_custom")

 ;; time in days
 ;; dist in miles
 ;; categories: Home=All
 (define booktour-search
   (lambda (zipcode distance category time)
     (let ((parms `(("distance" . ,distance)
		  ("zip"      . ,zipcode)
		  ("category" . ,category)
		  ("time"     . ,time))))       
       (let ((uri (make-uri "http" #f booktour-host #f booktour-path (parms->query parms) "")))
	 (let-values (((hdrs hip) (http-invoke 'GET uri `(,(host-header booktour-host)))))
		   (let ((http-resp (parse-http-response-line (car hdrs))))
		     (if (string=? (response-line-code http-resp) "200")
			(let ((tip (make-ascii-input-port hip)))
			  (let ((results (xml->sxml tip '())))
			    (close-port tip)
			    results))
			'())))))))
 
 )
;; http://booktour.com/readers/rss_custom/?distance=50&zip=33496&category=Home&time=28