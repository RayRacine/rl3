(library
 (rl3 api feedburner)
 
 (export 
  feedburner-rss)
 
 (import 
  (rnrs base)
  (rnrs exceptions)
  (rnrs conditions)
  (rnrs io simple) ;; debug only
  (rnrs io ports)
  (only (rl3 io print)
	pretty-print)  
  (only (rl3 web rss rss20 rss)
	fetch-rss)
  (only (rl3 web uri)
	make-uri)
  (only (rl3 web uri url parms)
	parms->query))
  
 (define feedburner-host
   "feedproxy.feedburner.com")
 
 (define parms (parms->query '()))
 
 (define feedburner-rss
   (lambda (feed)
     (let ((uri (make-uri "http" #f  feedburner-host #f feed parms "")))
       (fetch-rss uri))))

 )
