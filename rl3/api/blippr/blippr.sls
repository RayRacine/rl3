(library 
 (rl3 api blippr blippr)

 (export 
  parse-content-encoded
  blippr-book-rss)

 (import
  (rnrs base)
  (rnrs io ports)
  (only (rl3 web uri)
	make-uri)
  (only (rl3 web uri url parms)
	parms->query)
  (only (rl3 web rss rss20 rss)
	fetch-rss)
  (only (rl3 web http resource)	
	fetch-resource-xml)
  (only (rl3 xml ssax ssax)
	xml->sxml))

 (define parse-content-encoded
   (lambda (content)
     (xml->sxml (open-string-input-port content) '())))

 (define blippr-book-rss-url
   (make-uri "http" #f "www.blippr.com" #f "/books/blips.rss" (parms->query '()) ""))

 (define blippr-book-rss
   (lambda ()
     (fetch-resource-xml blippr-book-rss-url '() (lambda () 'ERROR))))

 )
