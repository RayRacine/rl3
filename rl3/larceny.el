;;; larceny.el --- enhanced support for editing and running Scheme code in Larceny

(defconst larceny-copyright    "Copyright (C) 2008 Raymond Paul Racine")
(defconst larceny-copyright-2  "Portions Copyright (C) Free Software Foundation")

;; Emacs-style font-lock specs adapted from GNU Emacs 21.2 scheme.el.
;; Scheme Mode menu adapted from GNU Emacs 21.2 cmuscheme.el.
;; Many ideas and code  borrowed from Neil W. Van Dyke's quack.el

(defconst larceny-version      "0.01")
(defconst larceny-author-name  "Raymond Racine")
(defconst larceny-author-email "ray.racine@gamail.com")

(defconst larceny-legal-notice
  "This is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2, or (at your option) any later version.  This is
distributed in the hope that it will be useful, but without any warranty;
without even the implied warranty of merchantability or fitness for a
particular purpose.  See the GNU General Public License for more details.  You
should have received a copy of the GNU General Public License along with Emacs;
see the file `COPYING'.  If not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.")

(require 'cmuscheme)

;; Install enhancements into the scheme-mode keymap
(define-key scheme-mode-map "\C-c\C-b" 'larceny-send-buffer)
(define-key scheme-mode-map "\C-c\C-d" 'larceny-send-eof)

(defgroup larceny nil
  "Enchanced suppport for Larceny scheme."
  :group  'scheme
  :prefix "larceny-")

;; Taken from Neil's Quack.
(defcustom larceny-pretty-lambda nil
  "*Whether Quack should display \"lambda\" as the lambda character."
  :type       'boolean
  :group      'larceny)
;;  :set        'quack-custom-set
;;  :initialize 'custom-initialize-default)

;; Pretty Lambda and delta  character
(defconst larceny-lambda-char    (decode-char 'ucs #x03BB))      ;; (make-char 'greek-iso8859-7 107)) 
(defconst larceny-delta-char     (decode-char 'ucs #x03B4))      ;; (make-char 'greek-iso8859-7 100)) 
(defconst larceny-beta-char      (decode-char 'ucs #x03B2))      ;; 'greek-iso8859-7 98))
(defconst larceny-rho-char       (decode-char 'ucs #x03C1))
(defconst larceny-sigma-char     (decode-char 'ucs #x03C2))
(defconst larceny-iota-char      (decode-char 'ucs #x03B9))
(defconst larceny-log-and-char   (decode-char 'ucs #x2227))
(defconst larceny-log-or-char    (decode-char 'ucs #x2228))
(defconst larceny-log-not-char   (decode-char 'ucs #x00AC))
(defconst larceny-log-true-char  (decode-char 'ucs #x22A4))
(defconst larceny-log-false-char (decode-char 'ucs #x22A5))
(defconst larceny-less-equal-char    (decode-char 'ucs #x2264))
(defconst larceny-greater-equal-char (decode-char 'ucs #x2265))

;;;;;;;;;;;;;;;;
;; Font Faces ;;
;;;;;;;;;;;;;;;;
(defface  larceny-paren-face
  '((((class color) (background light)) (:foreground "red3"))
    (((class color) (background dark))  (:foreground "gray65"))
    (((class grayscale))                (:foreground "gray"))
    (t                                  ()))
  "Face used for parentheses."
  :group 'larceny)

(defface  larceny-dangerous-face
  '((((class color) (background light)) (:foreground "red3"))
    (((class color) (background dark))  (:foreground "red1"))
    (((class grayscale))                (:foreground "gray"))
    (t                                  ()))
  "Face used for baaad things like set!."
  :group 'larceny)

(defface  larceny-character-face
  '((((class color) (background light)) (:foreground "goldenrod4"))
    (((class color) (background dark))  (:foreground "goldenrod1"))
    (((class grayscale))                (:foreground "gray"))
    (t                                  ()))
  "Face used for scheme characters"
  :group 'larceny)

;; Send an eof to Larcney scheme process
;; Used for example to exit the debugger.
(defun larceny-send-eof ()
  "Send EOF to Larceny process."
  (interactive)
  (if (scheme-proc)
      (with-current-buffer scheme-buffer
	(comint-send-eof))))    

(defun larceny-send-buffer ()
  "Send the buffer contents to Larceny process"
  (interactive)
  (scheme-send-region (point-min) (point-max)))

;; Add send buffer to Scheme menu
(let ((map (lookup-key scheme-mode-map [menu-bar scheme])))
  (define-key map [send-buffer] '("Evaluate Buffer" . larceny-send-buffer)))

;; imenu 
(setq larceny-imenu-generic-expression
      '(("Syntax"    "^(define-syntax *\\(.*\\)" 1)
	("Defines"
	 "^\\s-*(define\\s-+(?\\(\\sw+\\)" 1)))

(global-set-key [S-mouse-3] 'imenu) 

;; Functions which communicate to the Larceny Repl

(defun larceny-debug-enable ()
  "Toggle Larceny debugger support on."
  (interactive)
  (comint-send-string (scheme-proc) "(import (rl3 env debug))(debug-enable #t)\n")
  (comint-send-string (scheme-proc) "\n")
  (message "Larceny debugger enabled"))

(defun larceny-debug-disable ()
  "Toggle Larceny debugger support off."
  (interactive)
  (comint-send-string (scheme-proc) "(import (rl3 env debug))(debug-enable #f)\n")
  (message "Larceny debugger disabled"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Keywords special to Larceny or non-standard scheme or non-R6RS extensions. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconst larceny-key-words
  '(("[[(]\\(case-\\|match-\\|opt-\\)?\\(lambda\\)\\>"
     2
     (progn (compose-region (match-beginning 2)
			    (match-end       2)
			    larceny-lambda-char)
	    nil))
    ("[[(]\\(define\\)\\(-.+\\)?\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-delta-char)
	    nil))
    ("[[(]\\(let\\)\\(*\\|-\\)?"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-beta-char)
	    nil))
    ("[[(]\\(set!\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-sigma-char)
	    nil))
    ("[[(]\\(begin\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-rho-char)
	    nil))
    ("[[(]\\(if\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-iota-char)
	    nil))
    ("[[(]\\(and\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-log-and-char)
	    nil))
    ("[[(]\\(or\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-log-or-char)
	    nil))
    ("[[(]\\(not\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-log-not-char)
	    nil))
    ("[[(]\\(<=\\)"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-less-equal-char)
	    nil))
    ("[[(]\\(>=\\)"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-greater-equal-char)
	    nil))
    ("\\(#t\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-log-true-char)
	    nil))
    ("\\(#f\\)\\>"
     1
     (progn (compose-region (match-beginning 1)
			    (match-end       1)
			    larceny-log-false-char)
	    nil))
    ("[][()]"    . 'larceny-paren-face)
    ("#[\\]\\w+" . 'larceny-character-face)       ;; characters
    ("#x\\w+"    . 'larceny-character-face)       ;; hex numbers
    ("\\w+set!"  . 'larceny-dangerous-face)
    ("\\w+!"     . 'larceny-dangerous-face)
    "library" "import" "export" "not" "#t" "#f" "aif" "define-record-type"))

;;     1
;;     (1 font-lock-keyword-face)
;;     (2 font-lock-type-face nil t))))


(defun larceny-install-fontification ()
  "Install font lock extendsions to scheme mode."
  (font-lock-add-keywords nil larceny-key-words))

;; (defcustom larceny-exe "larceny"
;;   "Path to larceny repl."
;;   :group 'larceny
;;   :type  'string)

;; (defcustom larceny-import-path "."
;;   "Larceny path for requires or imports."
;;   :group 'larceny
;;   :type  'string)

;; (defadvice run-scheme (around larceny-run-scheme-adv first nil activate)
;;   "Launch Larceny if necessary by advice around run-scheme."
;;   (let ((buf (current-buffer))
;; 	(wg  (current-window-configuration)))
;;     (setq scheme-program-name (concat larceny-exe " -path " larceny-import-path " -err5rs"))
;;     ad-do-it
;;     (set-window-configuration wg)
;;     (set-buffer buf)
;;     (message "Started Larceny with -path %s"  larceny-import-path))
;;   (pop-to-buffer "*scheme*"))

(add-hook 'scheme-mode-hook
	  (lambda ()
	    (setq imenu-generic-expression larceny-imenu-generic-expression)
	    (larceny-install-fontification)))

(add-hook 'scheme-mode-hook 'imenu-add-menubar-index)

(provide 'larceny
)