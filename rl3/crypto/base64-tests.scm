(import
 (rnrs base)
 (only (rnrs bytevectors)
       utf8->string string->utf8)
 (only (rnrs control)
       when)
 (rl3 env debug)
 (rl3 test unit-tests)
 (rl3 crypto base64))

(debug-enable #f)

(begin

  (when (test-runner-current)
    (test-runner-reset (test-runner-current)))

  (test-begin "Base64 Encoding and Decoding - Test Suite")

  (test-begin "1. Encode")

  ;; From Wikipedia entry on Base64
  (test-equal? "Wikipedia entry on Base64, Hobbes" (base64-encode (string->utf8 "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure."))
              "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=")

  (test-assert "2 Pad '='" (let ((s (reverse (string->list (base64-encode (string->utf8 "1"))))))
                             (and (eqv? (car s) #\=)
                                  (eqv? (cadr s) #\=))))

  (test-assert "1 Pad '='" (let ((s (reverse (string->list (base64-encode (string->utf8 "12"))))))
                             (and (eqv? (car s) #\=)
                                  (not (eqv? (cadr s) #\=)))))

  (test-assert "0 Pad '='" (let ((s (reverse (string->list (base64-encode (string->utf8 "123"))))))
                             (not (or (eqv? (car s) #\=)
                                       (eqv? (cadr s) #\=)))))
  
  (test-end "1. Encode")

  
  (test-begin "2. Decode")

  ;; NOTE DOES NOT WORK AS LARCENY HAS BUG (SKIPS nth CHAR) in get-string-n!
  (test-equal? "Decode - Wikipedia entry on Base64, Hobbes"
               (utf8->string (base64-decode "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4="))
               "Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.")


  
  (test-end  "2. Decode")
  
  (test-end "Base64 Encoding and Decoding - Test Suite")

  )
